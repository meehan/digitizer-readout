# Digitizer DAQ

The [CAEN vx1730](https://www.caen.it/products/vx1730/) digitizer board is used 
in conjunction with the [Struck sis3153](https://www.struck.de/sis3153.html) interface
board to read out the calorimeter and scintillator PMTs for FASER.  These two boards
are controlled by the software housed here.  

## Documentation

### System Specific

Documentation can be found on the above sites but it can be challenging to know precisely
which documents you should read.  If in doubt, the following documents are reliable resources
that are useful to understand the details of the hardware :
- sis3153 Manual : [sis3153-m-usb-1-v106-manual.pdf](https://faserdaq.web.cern.ch/faserdaq/sis3153-m-usb-1-v106-manual.pdf)
- sis3153 Ethernet Addendum : [sis3153-m-eth-1-v107-ethernet-addendum.pdf](https://faserdaq.web.cern.ch/faserdaq/sis3153-m-eth-1-v107-ethernet-addendum.pdf)
- vx1730 Registers : [UM5118_725-730__Registers_Description_rev2.pdf](https://faserdaq.web.cern.ch/faserdaq/UM5118_725-730__Registers_Description_rev2.pdf)
- vx1730 Manual : [UM2792_V1730_V1725_rev2.pdf](https://faserdaq.web.cern.ch/faserdaq/UM2792_V1730_V1725_rev2.pdf)

### Generic DAQ [Under Construction]

If you are new to data acquisition, a few useful references can be found at the links below:
- VMEbus
- UDP

## Software Setup

**IMPORTANT** Unless you are a developer, please use the tag : [v0.1](https://gitlab.cern.ch/faser/digitizerreadout/-/tags/v0.1)

The directions here describe the basic setup, but the precise output when you run the code (because
it will have been developed) may be slightly different. The software is built using [cmake](https://cmake.org)
but the precise way you perform this build differs depending on whether you are running this as a standalone
setup from the command line, or whether you are running it within the [daqlinq](https://gitlab.cern.ch/ep-dt-di/daq/daqling).

### Standalone

Start by cloning the [digitizerreadout](.) repository.  Note that it is necessary to clone the submodules contained within
the repository.  This is done using the `git submodule init` and `git submodule update` command as shown below. This can also 
be done by cloning the repository recursively (`--recursive`) if you want.

```
[sam@faser-daq-001 ~]$ git clone ssh://git@gitlab.cern.ch:7999/faser/digitizerreadout.git
Cloning into 'digitizerreadout'...
remote: Enumerating objects: 138, done.
remote: Counting objects: 100% (138/138), done.
remote: Compressing objects: 100% (87/87), done.
remote: Total 655 (delta 78), reused 97 (delta 51)
Receiving objects: 100% (655/655), 1.41 MiB | 0 bytes/s, done.
Resolving deltas: 100% (407/407), done.
```

```
[sam@faser-daq-001 ~]$ cd digitizerreadout/
[sam@faser-daq-001 digitizerreadout]$ git submodule init
Submodule 'json' (https://github.com/nlohmann/json.git) registered for path 'json'
```

```
[sam@faser-daq-001 digitizerreadout]$ git submodule update
Cloning into 'json'...
remote: Enumerating objects: 1, done.
remote: Counting objects: 100% (1/1), done.
remote: Total 49728 (delta 0), reused 0 (delta 0), pack-reused 49727
Receiving objects: 100% (49728/49728), 177.67 MiB | 21.23 MiB/s, done.
Resolving deltas: 100% (40339/40339), done.
Submodule path 'json': checked out '65e4b973bdc04420335f6e76950f53632ed903c2'
```

**IMPORTANT** : If you are not a developer, you will also need to checkout the stable "user-friendly" version of the code that is specified
above.  This can be done via the `git checkout` command for the tagged branch (What precisely this branch is is written above).

Next create your build directory and configure the code.  Note that if you are compiling locally, then you need to 
define the environment variable `LOCAL` defined in the cmake call so that it uses the local calls to logging functions.  Otherwise,
it will be assuming that you are building it within the context of daqling.
```
[root@pcatbnl2 FASER]# cd digitizerreadout
[root@pcatbnl2 FASER]# mkdir build
[root@pcatbnl2 build]# cd build
[root@pcatbnl2 build]# cmake -DLOCAL=True ../
-- The C compiler identification is GNU 4.4.7
-- The CXX compiler identification is GNU 4.4.7
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /root/FASER/build
```

Now build the code
```
[root@pcatbnl2 build]# make
Scanning dependencies of target sis3153-core
[ 25%] Building CXX object CMakeFiles/sis3153-core.dir/sis3153-core/sis3153ETH_vme_class.cpp.o
[ 50%] Linking CXX static library libsis3153-core.a
[ 50%] Built target sis3153-core
Scanning dependencies of target fdaq-ReadBoardInfo
[ 75%] Building CXX object CMakeFiles/fdaq-ReadBoardInfo.dir/faser-daq/sis3153_test0.cpp.o
[100%] Linking CXX executable fdaq-ReadBoardInfo
[100%] Built target fdaq-ReadBoardInfo
```
You are now ready to run any of the executables which have been written and included in 
the CMakeLists.txt file for compilation into executables.

### Within Daqling
The daqling setup that this code is intended to be used with is that of FASER and can
be found here - [faser/daq](https://gitlab.cern.ch/faser/daq).  As such, you will need
to build that code and there is separate documentation that may be more up to date
in that repository.  Included here are the minimal directions that have been used
by some.

**NOTE** : These directions are written for the "sam_digitizer" branch.  If you
are not Sam, please don't push directly to this branch.

Start by recursively cloning the faser/daq repository.
```
git clone --recursive https://gitlab.cern.ch/faser/daq.git
```
Next, you will have to go in and `init` and `update` a couple of submodules. This
will get the (digitizerreadout)[.] repository :
```
cd daq
git submodule init
git submodule update
```
and then this will get the necessary [json/nlohmann](https://github.com/nlohmann/json) repository which is
included as a submodule
```
cd daq/digitizerreadout
git submodule init
git submodule update
```
Now go back into the top level `daq` directory that you initially cloned and
build this within a build directory. **NOTE** that now, you are :
- Using `cmake3` instead of cmake as above.  Though you could have used cmake3 if you have it.
- Not using the `-DLOCAL` flag during the call to cmake.

```
mkdir build
cd build
cmake3 ../
make -j8
```
You will now be able to start up the `daqinterface` and on the "sam_digitizer"
branch there is even a config you can use
```
cd daq
daqpy.py configs/emulatorLocalhost_withDigitizer.json complete
```

## Network Routing
This software is intended to be used with the setup that has as a central hub a switch 
that connects to :
- CERN network
- PC
- VME crate

Start by querying what networks exist and are in communication with the board by using the 
`ifconfig` command, which should print something like the following.  From this, you can see
that the `em1` network, with an ip address of `128.141.48.75` is the active network that
we will be using to route the signals from the PC to the interface board :
```
[root@faser-daq-001 build]# ifconfig
em1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 128.141.48.75  netmask 255.255.255.192  broadcast 128.141.48.127
        inet6 fe80::7a2b:cbff:fe71:b110  prefixlen 64  scopeid 0x20<link>
        inet6 2001:1458:202:2::101:b6de  prefixlen 128  scopeid 0x0<global>
        ether 78:2b:cb:71:b1:10  txqueuelen 1000  (Ethernet)
        RX packets 2853  bytes 285688 (278.9 KiB)
        RX errors 0  dropped 1  overruns 0  frame 0
        TX packets 1915  bytes 291667 (284.8 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

em2: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        ether 78:2b:cb:71:b1:11  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 32  bytes 2484 (2.4 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 32  bytes 2484 (2.4 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:76:ef:11  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

[root@faser-daq-001 build]#
```
After knowing this, we can set up the internal local network ip routing.  Start by mapping the 
MAC address of the interface board to a unique ip address using the [arp command](https://www.computerhope.com/unix/arp.htm).
Note that in this command, we specify that the `-i` (interface) is within the `em1` network (like above) and
that the ip address to which we will assign the hardware address of the board is `128.141.48.92` (just make that
last number unique).  This
is slightly different than the ip of the `em1` network above and will function as the network *for our system*.
```
arp -i em1 -s 128.141.48.92 00:00:56:15:30:A2
```
You should now see this appear in the table of mapped devices
```
[root@faser-daq-001 build]# arp -n
Address                  HWtype  HWaddress           Flags Mask            Iface
128.141.48.65            ether   0a:00:30:b5:e1:41   C                     em1
128.141.48.92            ether   00:00:56:15:30:a2   CM                    em1
[root@faser-daq-001 build]#
```

Now, add an appropriate routing to specify the network on which the above ip address 
operates using the [route command](https://www.computerhope.com/unix/route.htm).  Again, you
need to specify the proper interface (in this case `em1`) as well as the same ip address that
you set up before.
```
ip route add 128.141.48.92 dev em1
```

If this has worked, then you should be able to ping the ip address of the interface board
```
ping 128.141.48.92
```
and see a positive response returned and the "L" LED light blink on the board.

*However*, if you see the "L" LED blinking at a very rapid (4 Hz) rate, then this
indicates that the ethernet connection is non-existent.  In this case, you should 
consider checking that the physical connections have been made properly (i.e. did a cord
fall out?).

