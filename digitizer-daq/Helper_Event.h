#ifndef  HELPER_EVENT_INCLUDE_H
#define  HELPER_EVENT_INCLUDE_H

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <cstring>
#include <memory>
#include <fstream>

#include "Helper.h"

#include "EventFormats/DAQFormats.hpp"

using namespace DAQFormats;

#define MAXFRAGSIZE 16000
#define NCHANNELS 16

// emulation of the FASER-DAQ event fragment structures
// remove this later
// enum EventMarkers {
//   FragmentMarker = 0xAA,
//   EventMarker = 0xBB
// };

enum DumpMode {
 New,
 Append
};


int Payload_GetEventSize(const uint32_t raw_payload[], bool debug=false );
int Payload_GetChannelMask(const uint32_t raw_payload[], std::vector< bool >& channel_mask, bool debug=false );
int Payload_GetBufferLength(const uint32_t raw_payload[], int& words_per_channel, int& samples_per_channel, bool debug);
int Payload_Dump(const uint32_t raw_payload[], bool debug=false );
int Payload_Parse(const uint32_t raw_payload[], std::vector< std::vector< unsigned int >>& data_parsed, bool debug=false);

int Data_Dump( std::unique_ptr<EventFragment>& data, int n_dump, bool debug=false );
int Data_Write( std::unique_ptr<EventFragment>& data, std::string outputpath, DumpMode mode = DumpMode::New, bool debug=false );

#endif



