//============================================================================
// Author : Sam Meehan <samuel.meehan@cern.ch>
//
// Description : Register shorthands for the master control board
//
// Relevant documentation : 
//  - https://faserdaq.web.cern.ch/faserdaq/sis3153-m-eth-1-v107-ethernet-addendum.pdf
//
//============================================================================


#ifndef  REGISTERS_SIS3153_INCLUDE_H
#define  REGISTERS_SIS3153_INCLUDE_H

#define SIS3153_CONTROL_STATUS              0x0
#define SIS3153_MODID_VERSION               0x1
#define SIS3153_SERIAL_NUMBER_REG           0x2
#define SIS3153_LEMO_IO_CTRL_REG            0x3
#define SIS3153_VME_MASTER_CONTROL_STATUS   0x10
#define SIS3153_VME_MASTER_CYCLE_STATUS     0x11
#define SIS3153_VME_INTERRUPT_STATUS        0x12
#define SIS3153_KEY_RESET_ALL               0x0100

#endif