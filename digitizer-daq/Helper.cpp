//============================================================================
// Author : Sam Meehan <samuel.meehan@cern.ch>
//
// Description : Helper functions, but which are not hardware specific 
//
// Relevant documentation : NONE
//
//============================================================================


#include "Helper.h"


// for the generic input parsing
void genHelp(){

  DEBUG("This is the help screen for your vx1730 digitizer command line running");
  DEBUG("You must provide the following arguments : ");
  DEBUG(" -c [CONFIGFILE] : CONFIGFILE is the path to the json formatted config file");
  DEBUG(" -o [OUTPUTPATH] : OUTPUTPATH is the location of the directory that you wish");
  DEBUG("                   to use to store your output files");

}

json openJsonFile(std::string filepath){
  INFO("Openning config : "<<filepath);
  std::ifstream file(filepath);
  if (!file) {
    throw std::runtime_error("Could not open json config file : "+filepath);
  }
  json j;
  try {
    j = json::parse(file);
  }catch (json::parse_error &e) {
    throw std::runtime_error(e.what());
  }
  file.close();
  return j;
}

std::string ConvertIntToWord(unsigned int word, bool debug){
  // converts an unsigned int to a string of 0's and 1's formatted
  // in an appropriate manner as a 32 bit word

  std::string wordout;

  for(int iBit=31; iBit>=0; iBit--){

    int bitval = (word & (1<<iBit)) >> iBit;

    if(debug)
      INFO("bitval : "<<std::dec<<iBit<<"  "<<bitval);

    wordout += std::to_string(bitval);

    if(iBit%4==0)
      wordout += " ";

  }

  if(debug)
    INFO("wordout : "<<wordout);

  return wordout;
}

void SetBit(unsigned int& word, int bit_location, int bit_val){
  // used to flip a single bit to a particular value

  if(bit_val==0){
      // n=3
      // prev      = 01101011 
      // mask      = 11110111 [~(1<<3)]
      // prev&mask = 01100011 [only bit 3 has changed]
      word &= ~( 1 << bit_location );
  }
  else if(bit_val==1){
      // n=4
      // prev      = 01101011 
      // mask      = 00010000 [(1<<4)]
      // prev|mask = 01111011 [only bit 4 has changed]
      word |=  ( 1 << bit_location );
  }
  else{
      INFO("This is not a valid bit value to set - exitting");
      exit(18);
  }

}

int GetBit(unsigned int word, int bit_location){
  // obtain the value of a particular bit in a word

  word =  (word>>bit_location);
  word &= 0x1;
  
  return word;
}

std::string GetDateVerboseString(){

  std::string verboseDate;
  
  using namespace std;
  using namespace std::chrono;
  typedef duration<int, ratio_multiply<hours::period, ratio<24> >::type> days;
  system_clock::time_point now = system_clock::now();
  system_clock::duration tp = now.time_since_epoch();
  days d = duration_cast<days>(tp);
  tp -= d;
  hours h = duration_cast<hours>(tp);
  tp -= h;
  minutes m = duration_cast<minutes>(tp);
  tp -= m;
  seconds s = duration_cast<seconds>(tp);
  tp -= s;
  INFO(d.count() << "d " << h.count() << ':' << m.count() << ':' << s.count());
  INFO(" " << tp.count() << "[" << system_clock::duration::period::num << '/' << system_clock::duration::period::den << "]");

  time_t tt = system_clock::to_time_t(now);
  tm utc_tm = *gmtime(&tt);
  tm local_tm = *localtime(&tt);
  INFO(utc_tm.tm_year+1900);
  INFO(utc_tm.tm_mon+1);
  INFO(utc_tm.tm_mday);
  INFO(utc_tm.tm_hour);
  INFO(utc_tm.tm_min);
  INFO(utc_tm.tm_sec);
  
  //desciptor
  verboseDate += "Date";
  
  // year
  verboseDate += std::to_string( utc_tm.tm_year+1900 );
  
  // month
  if(utc_tm.tm_mon+1 < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_mon+1 );
  }
  else
    verboseDate += std::to_string( utc_tm.tm_mon+1 );
    
  // day
  if(utc_tm.tm_mon+1 < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_mday );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_mday );
  }
  
  //desciptor
  verboseDate += "_Time";
  
  // hour
  if(utc_tm.tm_hour < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_hour );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_hour );
  }

  // minute
  if(utc_tm.tm_min < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_min );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_min );
  }

  // second
  if(utc_tm.tm_sec < 10){
    verboseDate += "0";
    verboseDate += std::to_string( utc_tm.tm_sec );
  }
  else{
    verboseDate += std::to_string( utc_tm.tm_sec );
  }


  return verboseDate;
}

void Wait(float nseconds){
    INFO("Waiting for : "<<nseconds<<" seconds");
    int nmicroseconds = floor(nseconds*1000000.0);
    usleep(nmicroseconds);
}