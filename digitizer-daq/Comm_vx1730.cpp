//============================================================================
// Author : Sam Meehan <samuel.meehan@cern.ch>
//
// Description : Helper functions, primarily for digitizer configuration
//
// Relevant documentation :
//  - http://faserdaq.web.cern.ch/faserdaq/UM2792_V1730_V1725_rev2.pdf
//  - https://faserdaq.web.cern.ch/faserdaq/UM5118_725-730__Registers_Description_rev2.pdf
//
//============================================================================

#include "Comm_vx1730.h"

/// TODO : Add doxygen comments
/// 
///

vx1730::vx1730(char ip[], unsigned int vme_base){
  INFO("vx1730::Constructor");
      
  // open the vme crate connection
  // the vme_crate is a member of this class 
  // and we are connecting it with an IP address here
  sis3153eth(&crate, ip);  
  
  int return_code;
  char char_messages[128] ;
  unsigned int nof_found_devices ;

  // open Vme interface
  return_code = crate->vmeopen();  
  
  // open Vme interface
  crate->get_vmeopen_messages(char_messages, &nof_found_devices);  

  
  INFO("Openning VME connection : ");
  INFO("get_vmeopen_messages = "<<setw(20)<<char_messages);
  INFO("nof_found_devices    = "<<setw(20)<<nof_found_devices);
    
  // test that the communication is open
  sis3153_TestComm( crate, true ); 
    
  // set the base address of the single vx1730 card itself
  // would need to extend this in the case of multiple cards
  base_address = vme_base;
    
}

vx1730::~vx1730(){
  INFO("vx1730::Destructor");
}

int vx1730::TestComm(){
  INFO("vx1730::TestComm");

  int return_code;
  unsigned int data;
  unsigned int addr;

  INFO("Testing read/write to scratch space on digitizer");

  // testing scratch space
  ReadSlaveReg(crate, base_address+VX1730_SCRATCH, data );
  WriteSlaveReg(crate, base_address+VX1730_SCRATCH, 0x22222222 );
  ReadSlaveReg(crate, base_address+VX1730_SCRATCH, data );
  WriteSlaveReg(crate, base_address+VX1730_SCRATCH, 0x10101010 );
  ReadSlaveReg(crate, base_address+VX1730_SCRATCH, data );

  // testing scratch space on each channel
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, data );
  WriteSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, 0x55555555 );
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, data );
  WriteSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, 0x30303030 );
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DUMMY32, data );

  INFO("Check Fixed Configurations");
  // three registers that should be fixed
  // ToDo: check that they match manufacturer specs
  ReadSlaveReg(crate, base_address+VX1730_CONFIG_ROM, data );
  ReadSlaveReg(crate, base_address+VX1730_CONFIG_ROM_BOARD_VERSION, data );
  ReadSlaveReg(crate, base_address+VX1730_CONFIG_ROM_BOARD_FORMFACTOR, data );
  ReadSlaveReg(crate, base_address+VX1730_ROC_FPGA_FW_REV, data );

  INFO("Check firmware on each digitizer channel");
  for(unsigned int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_AMC_FIRMWARE_VERSION+(0x0100*iChan), data );
  }

  return 0;
}

int vx1730::Configure( json config, bool debug){
  INFO("vx1730::Configure");
  
  INFO("INITIAL state before configuration");
  DumpConfig();
  
  ConfigReset(config, debug);
  
  ConfigBuffer(config, debug);
  
  ConfigReadoutEnable(config, debug);
  
  ConfigVoltageReading(config, debug);
  
  ConfigBlockReadout(config, debug);
  
  ConfigEventReadoutTrigger(config, debug);
  
  ConfigGroupTriggerSettings(config, debug);
  
  ConfigTriggerCoincidence(config, debug);
  
  ConfigFrontPanelTrigger(config, debug);

  ConfigLVDS(config, debug);
  
  INFO("FINAL state before configuration");
  DumpConfig();

  return 0;
}

int vx1730::ConfigReset( json config, bool debug){
  INFO("vx1730::ConfigReset");

  // reseting the SW will screw with the ability to write a DC offset 
  DEBUG("Reset system configurations to default");
  WriteSlaveReg(crate, base_address+VX1730_SW_RESET, 0x1111, debug );

  // wait to proceed until the channels are not busy
  bool reset_busy=true;
  while(reset_busy){
  
    // assume that the busy is finished and set it to be true if you find one channel to be busy
    reset_busy=false;
  
    // check each channel to see if it is busy while waiting for reset
    for(int iChan=0; iChan<16; iChan++){
      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_STATUS+(0x0100)*iChan, gen_data, true );
      DEBUG("CheckDC : "<<ConvertIntToWord(gen_data));
      if(GetBit(gen_data,2)){
        reset_busy=true;
      }
    }
    
    // the wait is necessary because it takes some time for the reset to propogate
    Wait(0.1);
  }

  DEBUG("Clear memory buffers");
  WriteSlaveReg(crate, base_address+VX1730_SW_CLEAR, 0x1111, debug );

  return 0;

}

int vx1730::ConfigBuffer( json config, bool debug){
  INFO("vx1730::ConfigBuffer");

  DEBUG("Number of buffers = "<<config["buffer_length"]);
  WriteSlaveReg(crate, base_address+VX1730_BUFFER_ORGANIZATION, std::stoi( std::string(config["buffer_length"]),0,16), debug );

  DEBUG("Post-sample length = "<<config["buffer_n_post_samples"]);
  WriteSlaveReg(crate, base_address+VX1730_POST_TRIGGER_SETTING, std::stoi( std::string(config["buffer_n_post_samples"]),0,16), debug );

  return 0;

}

int vx1730::ConfigReadoutEnable( json config, bool debug){\
  INFO("vx1730::ConfigReadoutEnable");
  
  unsigned int data_enable_mask;
  ReadSlaveReg(crate, base_address + VX1730_CHANNEL_EN_MASK, data_enable_mask, debug );
  DEBUG("Readout EnableMask : "<< ConvertIntToWord(data_enable_mask));
  
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){  
    SetBit(data_enable_mask, (int)config["channel_config"].at(iGrp)["chanA"]["channel"], (int)config["channel_config"].at(iGrp)["chanA"]["enable_read"]);
    SetBit(data_enable_mask, (int)config["channel_config"].at(iGrp)["chanB"]["channel"], (int)config["channel_config"].at(iGrp)["chanB"]["enable_read"]);
  }
  
  // writing enable mask
  WriteSlaveReg(crate, base_address + VX1730_CHANNEL_EN_MASK, data_enable_mask, debug );
  ReadSlaveReg(crate, base_address + VX1730_CHANNEL_EN_MASK, data_enable_mask, debug );
  DEBUG("Readout EnableMask : "<< ConvertIntToWord(data_enable_mask));

  return 0;
}

int vx1730::ConfigVoltageReading( json config, bool debug){
  INFO("vx1730::ConfigVoltageReading");
  
  DEBUG("Channel Dynamic Range and DC Offsets");

  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
  
    DEBUG("Group : "<<iGrp<<"  "<<config["channel_config"].at(iGrp)["group"]);
  
    int channel = 0;
    double dc_off  = 0;
    unsigned int dc_offset_bits;
    unsigned int channelStatus;
    double set_dynamic_range=2.0;
    
    for(int iChan=0; iChan<(int)m_chanPairs.size(); iChan++){
      DEBUG("Setting : "<<m_chanPairs.at(iChan));
      
      channel = (int)config["channel_config"].at(iGrp)[m_chanPairs.at(iChan)]["channel"];
      
      // dynamic range
      unsigned int dynamic_range_bit;
      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DYNAMIC_RANGE+(0x0100)*channel, dynamic_range_bit);
      if( (double)config["channel_config"].at(iGrp)[m_chanPairs.at(iChan)]["dynamic_range"]==0.5 ){
        SetBit(dynamic_range_bit, 0, 1);
      }
      else if( (double)config["channel_config"].at(iGrp)[m_chanPairs.at(iChan)]["dynamic_range"]==2.0 ){
        SetBit(dynamic_range_bit, 0, 0);
      }
      else{
        THROW(DigitizerHardwareException, "Not a valid choice for a dynamic range to set : "+to_string(dynamic_range_bit));
      }
      
      // dc offset
      dc_off  = (double)config["channel_config"].at(iGrp)[m_chanPairs.at(iChan)]["dc_offset"];    
      
      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_STATUS+(0x0100)*channel, gen_data, true );
      DEBUG("CheckDCRead : "<<ConvertIntToWord(gen_data));
      
      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DYNAMIC_RANGE+(0x0100)*channel, gen_data);
      if( GetBit(gen_data,0)==0 ){
        set_dynamic_range=2.0;
      }
      else if( GetBit(gen_data,0)==1 ){
        set_dynamic_range=0.5;
      }
      else{
        THROW(DigitizerHardwareException, "Not a valid choice for a dynamic range when inspected : "+to_string(GetBit(gen_data,0)));
      }
    
      if(abs(dc_off)>set_dynamic_range){
        THROW(DigitizerHardwareException, "This DC offset is too large : "+to_string(abs(dc_off)));
      }
      
      dc_offset_bits = (unsigned int)(floor(((dc_off + (set_dynamic_range/2.0))/set_dynamic_range) * 65536)-1.0);
      DEBUG("DC Offset Bits: "<<std::dec<<dc_offset_bits);
      
      // first check 0x1n88
      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_STATUS+(0x0100*channel), channelStatus, true );
      DEBUG("Status (bit [2] must be 0) : "<<ConvertIntToWord(channelStatus));
      
      // write and check the offset
      WriteSlaveReg(crate, base_address+VX1730_CHANNEL_DAC+(0x0100*channel), dc_offset_bits, true );
      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DAC+(0x0100*channel), dc_offset_bits, true );
      
      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_STATUS+(0x0100)*channel, gen_data, true );
      DEBUG("CheckDCRead : "<<ConvertIntToWord(gen_data));
    }    
  }

  return 0;
}

int vx1730::ConfigBlockReadout( json config, bool debug){
  INFO("vx1730::ConfigBlockReadout");

  // ToDo : Make this configurable for setting the number of events to be read out in a single block transfer
  WriteSlaveReg(crate, base_address + VX1730_BLT_EVENT_NB, 0x01, debug );
  ReadSlaveReg(crate, base_address + VX1730_BLT_EVENT_NB, gen_data, debug );
  
  // enable BERR to stop block transfer readout
  unsigned int data_readout_control;
  ReadSlaveReg(crate, base_address + VX1730_VME_CONTROL, data_readout_control, debug );
  DEBUG("Readout Control : "<<ConvertIntToWord(data_readout_control));
  
  SetBit(data_readout_control, 4, 1);
  
  WriteSlaveReg(crate, base_address + VX1730_VME_CONTROL, data_readout_control, debug );
  ReadSlaveReg(crate, base_address + VX1730_VME_CONTROL, data_readout_control, debug );
  DEBUG("Readout Control : "<<ConvertIntToWord(data_readout_control));
  
  return 0;
}

int vx1730::ConfigEventReadoutTrigger( json config, bool debug){
  INFO("vx1730::ConfigEventReadoutTrigger");

  // Global Trigger Mask
  // this is the generation of a global OR that will 
  // be used to generate an acquisition of a single event
  DEBUG("Trigger Global Sources"); 
  
  unsigned int trig_enable_mask;
  ReadSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  DEBUG("Initial TrigEnableMask : "<< ConvertIntToWord(trig_enable_mask));

  // enable each group separately
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
    int group  = (int)config["channel_config"].at(iGrp)["group"];
    int enable = (int)config["channel_config"].at(iGrp)["trig_enable"];
    SetBit(trig_enable_mask, group, enable);
  }

  // change the global features  
  DEBUG("LVDS     : "<<(int)config["global_trigger"]["lvds"]    );
  DEBUG("External : "<<(int)config["global_trigger"]["external"]);
  DEBUG("Software : "<<(int)config["global_trigger"]["software"]);

  SetBit(trig_enable_mask, 29, (int)config["global_trigger"]["lvds"]);
  SetBit(trig_enable_mask, 30, (int)config["global_trigger"]["external"]);
  SetBit(trig_enable_mask, 31, (int)config["global_trigger"]["software"]);

  DEBUG("Final TrigEnableMask : "<<std::hex<<trig_enable_mask);

  WriteSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  ReadSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  
  return 0;
}

int vx1730::ConfigGroupTriggerSettings( json config, bool debug){
  INFO("vx1730::ConfigGroupTriggerSettings");
  
  DEBUG("SelfTrigger Thresholds - once for each channel");
  
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
  
    DEBUG("Group : "<<iGrp<<"  "<<config["channel_config"].at(iGrp)["group"]);
  
    int channel = 0;
    unsigned int threshold = 0;
    unsigned int threshold_bits = 0;
    
    for(int iChan=0; iChan<(int)m_chanPairs.size(); iChan++){
      channel   = (int)config["channel_config"].at(iGrp)[m_chanPairs.at(iChan)]["channel"];
      threshold = (unsigned int)config["channel_config"].at(iGrp)[m_chanPairs.at(iChan)]["trig_threshold"];
    
      DEBUG(m_chanPairs.at(iChan)<<" : "<<std::dec<<channel<<"  "<<threshold);
      ReadSlaveReg(crate,  base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold_bits, true);
      WriteSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold, true);
      ReadSlaveReg(crate,  base_address+VX1730_CHANNEL_TRIG_THRESH + (0x0100)*channel, threshold_bits, true);
      DEBUG("Check thresh : "<<threshold_bits);
    }
  }


  DEBUG("SelfTrigger Polarity - once for full board");
  
  unsigned int channel_config;
  ReadSlaveReg(crate, base_address + VX1730_CHANNEL_CONFIG, channel_config, true );

  DEBUG("Initial ChannelConfig : "<< ConvertIntToWord(channel_config));

  DEBUG("Setting trigger polarity to default over - you will be notified if it is set to under");
  SetBit(channel_config, 6, 0);
  if(config["global_trigger"]["polarity"]=="under"){
    DEBUG("Setting trigger polarity to under");
    SetBit(channel_config, 6, 0);
  }

  DEBUG("Final ChannelConfig : "<< ConvertIntToWord(channel_config));

  WriteSlaveReg(crate, base_address + VX1730_CHANNEL_CONFIG, channel_config, true );
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_CONFIG, gen_data, debug );
  
  
  DEBUG("Trigger Group Logic and Output");
  
  // enable each group separately
  // note that iGrp is the counter but the retrieved [group] config value should be used within the loop
  // to specify the trigger group
  //
  // group = 0 --> chan(0,1)
  // group = 1 --> chan(0,1)
  // group = 2 --> chan(0,1)
  // group = 3 --> chan(0,1)
  // group = 4 --> chan(0,1)
  // group = 5 --> chan(0,1)
  // group = 6 --> chan(0,1)
  // group = 7 --> chan(0,1)
  
  for(int iGrp=0; iGrp<config["channel_config"].size(); iGrp++){
    // the logical group for the configuration
    // this will be translated later in the configuration itself
    int group  = (int)config["channel_config"].at(iGrp)["group"];
    
    // logic about how to combine the information from the two channels in a group
    DEBUG("Group : "<<group<<"  "<<std::string(config["channel_config"].at(iGrp)["trig_logic"]));
    
    unsigned int trig_chan_logic;
    ReadSlaveReg(crate, base_address + VX1730_CHANNEL_TRIG_LOGIC + (0x0100)*group, trig_chan_logic, true);
    DEBUG("ChanLogic Init : "<<ConvertIntToWord(trig_chan_logic));
    
    if( std::string(config["channel_config"].at(iGrp)["trig_logic"]).compare("AND")==0 ){
      SetBit(trig_chan_logic,0,0);
      SetBit(trig_chan_logic,1,0);
    }
    else if( std::string(config["channel_config"].at(iGrp)["trig_logic"]).compare("OnlyA")==0 ){
      SetBit(trig_chan_logic,0,1);
      SetBit(trig_chan_logic,1,0);
    }
    else if( std::string(config["channel_config"].at(iGrp)["trig_logic"]).compare("OnlyB")==0 ){
      SetBit(trig_chan_logic,0,0);
      SetBit(trig_chan_logic,1,1);
    }
    else if( std::string(config["channel_config"].at(iGrp)["trig_logic"]).compare("OR")==0 ){
      SetBit(trig_chan_logic,0,1);
      SetBit(trig_chan_logic,1,1);
    } 
    else{
      THROW(DigitizerHardwareException, "This is not a proper trigger logic configuration");
    }
    
    // ToDo : Make this configurable
    // always set bit[2]=0 so that the output trigger is of configurable width as programmed next
    SetBit(trig_chan_logic,2,0); // configurable width
//    SetBit(trig_chan_logic,2,1); // whenever above threshold
    
        
    DEBUG("ChanLogic Final : "<<ConvertIntToWord(trig_chan_logic));
    
    // note that the actual registers are in sets of two and so the iterator needs to be multiplied by 2
    WriteSlaveReg(crate, base_address + VX1730_CHANNEL_TRIG_LOGIC + (0x0100)*(2*group), trig_chan_logic, true);
    ReadSlaveReg(crate, base_address + VX1730_CHANNEL_TRIG_LOGIC + (0x0100)*(2*group), trig_chan_logic, true);


    // configured width of the trigger window output pulse length
    DEBUG("Group : "<<group<<"  "<<(int)config["channel_config"].at(iGrp)["trig_width"]);
    
    // loop over the two channels in that trigger group
    for(int iChan=0; iChan<(int)m_chanPairs.size(); iChan++){
    
      int channel = (int)config["channel_config"].at(iGrp)[m_chanPairs.at(iChan)]["channel"];
  
      unsigned int trig_chan_width;

      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*channel), trig_chan_width);
      DEBUG("TriggerGroup PulseWidth Initial : "<<trig_chan_width);

      int set_width_ns = 32;
      unsigned int set_width_clocks = 4;
  
      set_width_ns = (int)config["channel_config"].at(iGrp)["trig_width"];
  
      if(set_width_ns<0){
        THROW(DigitizerHardwareException, "The desired output trigger pulse must be positive but in the config it is negative : "+to_string(set_width_ns));
      }
      else if(set_width_ns>2048){
        THROW(DigitizerHardwareException, "The desired output trigger pulse width is too large (maximum width is 2048 ns = 256 clock cycles) : "+to_string(set_width_ns));
      }
      else{
        set_width_clocks = floor(set_width_ns/8.0);
      }
  
      DEBUG("Setting trigger output width : t="<<set_width_ns<<"  clocks="<<set_width_clocks);
  
      for(int iBit=0; iBit<8; iBit++)
        SetBit(trig_chan_width, iBit, 0);
    
      trig_chan_width |= set_width_clocks;

      WriteSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*channel), trig_chan_width);
      ReadSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*channel), trig_chan_width);
      DEBUG("TriggerGroup PulseWidth Final : "<<trig_chan_width);    
    }
  }
  
  return 0;
}

int vx1730::ConfigTriggerCoincidence( json config, bool debug){
  INFO("vx1730::ConfigTriggerCoincidence");

  unsigned int trig_enable_mask;
  ReadSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  DEBUG("Initial TrigEnableMask : "<< ConvertIntToWord(trig_enable_mask));

  // if you want to set a multichannel coincidence
  if( (bool)config["global_trigger"]["coincidence"] ){
    
    // how many channel-pairs enter the coincidence from among the enabled channels (set above)
    // the number in these three bits is one less than the number of channels in the coincidence
    if( (int)config["global_trigger"]["nchan"]<0 || (int)config["global_trigger"]["nchan"]>8 ){
      THROW(DigitizerHardwareException, "You can't have that number of coincidences : "+to_string((int)config["global_trigger"]["nchan"]));
    }
    else{
    
      DEBUG("trig_enable_mask1[bit] : "<<ConvertIntToWord(trig_enable_mask));
      
      // set the necessary bits to 0
      SetBit(trig_enable_mask, 26, 0);
      SetBit(trig_enable_mask, 25, 0);
      SetBit(trig_enable_mask, 24, 0);

      // make the mask for the OR
      unsigned int nchan = ( ((int)config["global_trigger"]["nchan"]-1) << 24);
      
      DEBUG("NChan[bit] : "<<ConvertIntToWord(nchan));
      
      // take the OR
      trig_enable_mask |= nchan;
    
      DEBUG("trig_enable_mask2[bit] : "<<ConvertIntToWord(trig_enable_mask));
    
    }
    
    // what is the duration of the coincidence window
    // in units of [nanoseconds] - rounding down to the nearest 8ns block
    // bits[23:20] = 8+4+2+1 = 15 is the max number of clock cycles
    // 15*8ns = 120ns
    if( (int)config["global_trigger"]["timewindow"]>120 ){
      THROW(DigitizerHardwareException, "This coincidence time window is too long (max = 120ns) : "+to_string((int)config["global_trigger"]["timewindow"]));
    }
    else{
      int nclock = floor( (int)config["global_trigger"]["timewindow"]/8.0 );
      DEBUG("NClock for Coincidence : "<<nclock);
      DEBUG("NClock[hex] : "<<std::hex<<nclock);
      
      DEBUG("trig_enable_mask1[bit] : "<<ConvertIntToWord(trig_enable_mask));
      
      // set the necessary bits to 0
      SetBit(trig_enable_mask, 23, 0);
      SetBit(trig_enable_mask, 22, 0);
      SetBit(trig_enable_mask, 21, 0);
      SetBit(trig_enable_mask, 20, 0);
      
      // make the mask for the OR
      unsigned int twin = (nclock<<20);
      
      DEBUG("TWin[bit] : "<<ConvertIntToWord(twin));
      
      // take the OR
      trig_enable_mask |= twin;
    
      DEBUG("trig_enable_mask2[bit] : "<<ConvertIntToWord(trig_enable_mask));
    }
    
  
  }
  else{
    DEBUG("You are not doing multi-channel coincidence in the global readout trigger");
  }

  DEBUG("Final TrigEnableMask : "<<std::hex<<trig_enable_mask);

  WriteSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );
  ReadSlaveReg(crate, base_address + VX1730_TRIG_SRCE_EN_MASK, trig_enable_mask, true );

  return 0;
}

int vx1730::ConfigFrontPanelTrigger( json config, bool debug){
  INFO("vx1730::ConfigFrontPanelTrigger");

  // Front Panel TRG-OUT (GPO) Enable Mask
  // this is the logical OR that generates the output signal 
  // to the Trg_Out on the front panel
  DEBUG("TrigOut Enable Mask");
  
  unsigned int trig_out_enable_mask = 0;
  ReadSlaveReg(crate, base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  
  // ToDo : currently hardcoded
  SetBit(trig_out_enable_mask,0,1);
  SetBit(trig_out_enable_mask,29,0);
  SetBit(trig_out_enable_mask,30,0);
  SetBit(trig_out_enable_mask,31,0);
  
  WriteSlaveReg(crate, base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  ReadSlaveReg(crate, base_address+VX1730_FP_TRIGGER_OUT_EN_MASK, trig_out_enable_mask, true );
  DEBUG("TrigOutEnable : "<< ConvertIntToWord(trig_out_enable_mask));
  
  return 0;

}

int vx1730::ConfigLVDS( json config, bool debug){
  INFO("vx1730::ConfigLVDS");

// #define VX1730_FP_IO_DATA          0x8118
// #define VX1730_FP_IO_CONTROL       0x811C  
// #define VX1730_FP_LVDS_IO_CTRL     0x81A0

  unsigned int lvds_control;
  ReadSlaveReg(crate, base_address+VX1730_FP_IO_CONTROL, lvds_control);
  DEBUG("VX1730_FP_IO_CONTROL (0x811C) : "<< ConvertIntToWord(lvds_control));
  
  SetBit(lvds_control, 0, 1); // LEMO trigger out is TTL (trig high)
  SetBit(lvds_control, 1, 0); // LEMO trigger enable - 0=enabled, 1=disabled 

  SetBit(lvds_control, 2, 1); // LVDS[0,3] set to output 
  SetBit(lvds_control, 3, 1); // LVDS[4,7] set to output 
  SetBit(lvds_control, 4, 0); // LVDS[8,11] set to output 
  SetBit(lvds_control, 5, 0); // LVDS[12,15] set to output 
  
  SetBit(lvds_control, 8, 1); // Enable new features (recommended by CAEN documentation) --> requires subsequent configuration of 0x81A0  
  
  WriteSlaveReg(crate, base_address+VX1730_FP_IO_CONTROL, lvds_control);
  ReadSlaveReg(crate, base_address+VX1730_FP_IO_CONTROL, lvds_control);
  DEBUG("VX1730_FP_IO_CONTROL (0x811C) : "<< ConvertIntToWord(lvds_control));

  
  unsigned int lvds_output_config;
  ReadSlaveReg(crate, base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  DEBUG("VX1730_FP_LVDS_IO_CTRL (0x81A0) : "<< ConvertIntToWord(lvds_output_config));
  
  // output of pins LVDS pins [3,0] which correspond to the associated trigger groups
  SetBit(lvds_output_config, 0, 1);
  SetBit(lvds_output_config, 1, 0);
  SetBit(lvds_output_config, 2, 0);
  SetBit(lvds_output_config, 3, 0);
  
  // output of pins LVDS pins [7,4] which correspond to the associated trigger groups
  SetBit(lvds_output_config, 4, 1);
  SetBit(lvds_output_config, 5, 0);
  SetBit(lvds_output_config, 6, 0);
  SetBit(lvds_output_config, 7, 0);  
  
  // NOTE : pins [15,8] are currently free and not being used for anything here
  // [1] could be used to send info out as to whether the buffer is getting full
  // [2] could be used to send info/commands back into the board which can periodically be read at register VX1730_FP_IO_DATA

  WriteSlaveReg(crate, base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  ReadSlaveReg(crate, base_address+VX1730_FP_LVDS_IO_CTRL, lvds_output_config);
  DEBUG("VX1730_FP_LVDS_IO_CTRL (0x811C) : "<< ConvertIntToWord(lvds_output_config));

  return 0;
}

int vx1730::ConfigInternalSawtooth( json config, bool debug){
  INFO("vx1730::ConfigInternalSawtooth");
  
  unsigned int channel_config;
  ReadSlaveReg(crate, base_address + VX1730_CHANNEL_CONFIG, channel_config, true );

  DEBUG("Initial ChannelConfig : "<< ConvertIntToWord(channel_config));

  DEBUG("SawTooth Enable : "<<config["internal_sawtooth"]);
  unsigned int sawtooth_enable;
  if(config["internal_sawtooth"]==true){
    DEBUG("Turning ON internal sawtooth waveform");
    SetBit(channel_config, 3, 1);
  }
  else if(config["internal_sawtooth"]==false){
    DEBUG("Turning OFF internal sawtooth waveform");
    SetBit(channel_config, 3, 0);
  }  

  DEBUG("Final ChannelConfig : "<< ConvertIntToWord(channel_config));

  WriteSlaveReg(crate, base_address + VX1730_CHANNEL_CONFIG, channel_config, true );
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_CONFIG, gen_data, debug );

  return 0;

}

int vx1730::Reset( bool debug){
  INFO("vx1730::Reset");

  // perform system reset
  DEBUG("Reset : System configurations");
  WriteSlaveReg(crate, base_address+VX1730_SW_RESET, 0x1111, debug );

  DEBUG("Reset : Clear memory buffers");
  WriteSlaveReg(crate, base_address+VX1730_SW_CLEAR, 0x1111, debug );

  return 0;
}

int vx1730::StartAcquisition( bool debug ){
  INFO("vx1730::StartAcquisition");

  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, gen_data, debug );
  DEBUG("StartAcquireState[1]: "<<ConvertIntToWord(gen_data));
  WriteSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, 0x04, debug );
  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, gen_data, debug );
  DEBUG("StartAcquireState[2]: "<<ConvertIntToWord(gen_data));

  // sleep for a short while before data taking starts
  usleep(10000);

  return 0;

}

int vx1730::StopAcquisition( bool debug ){
  INFO("vx1730::StopAcquisition");

  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, gen_data, debug );
  WriteSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, 0x00, debug );
  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_CONTROL, gen_data, debug );

  return 0;
}

int vx1730::DumpEventCount( bool debug ){
  DEBUG("vx1730::DumpEventCount");

  unsigned int n_events_in_buffer;
  ReadSlaveReg(crate, base_address + VX1730_EVENT_STORED, n_events_in_buffer, debug );

  unsigned int ev_size;
  ReadSlaveReg(crate, base_address + VX1730_EVENT_SIZE, ev_size, debug );

  unsigned int vme_status;
  ReadSlaveReg(crate, base_address + VX1730_VME_STATUS, vme_status, debug );

  unsigned int acquisition_status;
  ReadSlaveReg(crate, base_address + VX1730_ACQUISITION_STATUS, acquisition_status, debug );
  
  DEBUG("EVCount: ");
  DEBUG("n_events_in_buffer: "<<std::dec<<n_events_in_buffer);
  DEBUG("ev_size: "<<std::dec<<ev_size);
  DEBUG("vme_status: "<<std::hex<<vme_status);
  DEBUG("acquisition_status: "<<std::hex<<acquisition_status);

  return n_events_in_buffer;
}

int vx1730::SendSWTrigger( bool debug ){
  DEBUG("vx1730::SendSWTrigger");

  WriteSlaveReg(crate, base_address + VX1730_SW_TRIGGER, 0x1, debug );

  return 0;
}

int vx1730::MonitorTemperature( std::string outputfile, bool debug){
  INFO("vx1730::MonitorTemperature");

  ofstream fout;
  fout.open(outputfile, ios::out);

  // write data for each channel that is active and -1 if its not active
  fout<<setw(10)<<"reading";
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    std::string line = "chan-"+to_string(iChan);
    fout<<setw(10)<<line;
  }
  fout<<"\n";

  unsigned int data;
  int temperature;

  for(int iRead=0; iRead<30; iRead++){
    fout<<setw(10)<<std::dec<<iRead;
    for(int iChan=0; iChan<NCHANNELS; iChan++){
      ReadSlaveReg(crate, base_address + VX1730_CHANNEL_TEMPERATURE + 0x0100*iChan, data, debug );
      temperature = data & 0x000000FF;
      fout<<setw(10)<<std::dec<<temperature;
    }

    usleep(10000);
  }

  fout.close();

  return 1;
}

void vx1730::DumpConfig(){
  INFO("vx1730::DumpConfig");

  // reuse this same value for reading
  unsigned int data;

  // global information
  INFO("\n\n-- Global Info :");
  ReadSlaveReg(crate, base_address+VX1730_BUFFER_ORGANIZATION, data);
  INFO("BUFFER_ORGANIZATION  ("<<std::hex<<VX1730_BUFFER_ORGANIZATION<<") [hex]: 0x"<<std::hex<<data<<"  -  nsamples="<<std::dec<<GetBufferLength(data));

  ReadSlaveReg(crate, base_address+VX1730_POST_TRIGGER_SETTING, data);
  INFO("POST_TRIGGER_SETTING ("<<std::hex<<VX1730_POST_TRIGGER_SETTING<<") [dec]: "<<std::hex<<data);

  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_CONFIG, data);
  INFO("CHANNEL_CONFIG       ("<<std::hex<<VX1730_CHANNEL_CONFIG<<") [bit]: "<<std::hex<<ConvertIntToWord(data));

  ReadSlaveReg(crate, base_address+VX1730_ACQUISITION_CONTROL, data); 
  INFO("ACQUISITION_CONTROL  ("<<std::hex<<VX1730_ACQUISITION_CONTROL<<") [bit]: "<<std::hex<<ConvertIntToWord(data));

  // readout info
  INFO("\n\n-- Readout Enable :");
  ReadSlaveReg(crate, base_address+VX1730_CHANNEL_EN_MASK, data);
  INFO("CHANNEL_EN_MASK      ("<<std::hex<<VX1730_CHANNEL_EN_MASK<<") [bit]: "<<std::hex<<data);

  INFO("\n\n-- Channel DAC :");
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_DAC+(0x0100*iChan), data);
    INFO("CHANNEL_DAC "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_DAC+(0x0100*iChan)<<") [hex]: "<<std::hex<<data);
  }

  // trigger info
  INFO("Triggering :");
  ReadSlaveReg(crate, base_address+VX1730_TRIG_SRCE_EN_MASK, data);
  INFO("TRIG_SRCE_EN_MASK       ("<<std::hex<<VX1730_TRIG_SRCE_EN_MASK<<") [hex]: "<<std::hex<<data);
  INFO("TRIG_SRCE_EN_MASK       ("<<std::hex<<VX1730_TRIG_SRCE_EN_MASK<<") [bit]: "<<std::hex<<ConvertIntToWord(data));

  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_THRESH+(0x0100*iChan), data);
    INFO("TRIGGER_THRESH "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_TRIG_THRESH+(0x0100*iChan)<<") [hex]: "<<std::hex<<data);
  }

  for(int iChan=0; iChan<NCHANNELS; iChan++){
    ReadSlaveReg(crate, base_address+VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*iChan), data);
    INFO("TRIGGER_PULSE_WIDTH "<<iChan<<"      ("<<std::hex<<VX1730_CHANNEL_TRIG_PULSE_WIDTH+(0x0100*iChan)<<") [hex]: "<<std::hex<<data);
  }
}

int vx1730::ReadRawEvent( uint32_t raw_payload[], bool debug ){
  DEBUG("vx1730::ReadRawEvent");

  unsigned int addr;
  unsigned int data_nevents;
  unsigned int data_eventsize;
  unsigned int data_bltread;
  int return_code;

  // get the number of events
  ReadSlaveReg(crate, base_address + VX1730_EVENT_STORED, data_nevents, debug );
  ReadSlaveReg(crate, base_address + VX1730_EVENT_SIZE, data_eventsize, debug );
  ReadSlaveReg(crate, base_address + VX1730_BLT_EVENT_NB, data_bltread, debug );

  DEBUG("NEvents in Buffer : "<<std::dec<<data_nevents);
  DEBUG("Event Size        : "<<std::dec<<data_eventsize);
  DEBUG("BLT N Read        : "<<std::dec<<data_bltread);

  // read out event in its entirety using block read
  UINT gl_dma_buffer[MAXFRAGSIZE];
  UINT request_nof_words;
  UINT got_nof_words;

  // the size of the read will be the size of the event
  request_nof_words = data_eventsize;

  // initialize full internal buffer to 0
  for(int iWord=0; iWord<request_nof_words; iWord++) {
    gl_dma_buffer[iWord] = 0 ;
  }

  //////////////////////////////////////////
  // Block Read
  // must perform this read procedure multiple times as indicated in "Block Transfer D32/D64, 2eVME"
  //
  // each time you have to move the pointer forward as to where you put the data
  //////////////////////////////////////////
  
  int nwords_obtained=0; // this is the location in the data disk as to where you will be putting into the payload array
  while(true){
  
    // perform a single block read
    DEBUG("Reading into : "<<nwords_obtained);

    return_code = crate->vme_A32BLT32_read (base_address + 0x0000, &gl_dma_buffer[nwords_obtained], request_nof_words, &got_nof_words); //
    DEBUG("vme_A32BLT32_read:  addr = "+to_string(addr)+"   got_nof_words = "+to_string(got_nof_words)+"  return_code = "+to_string(return_code));
    
    // move forward the location where you will read into
    nwords_obtained += got_nof_words;
    
    // if you read and you get no words, then break out of loop
    if(got_nof_words==0){
      break;
    }
  }

  // copy the output of the BLT read to the payload object itself
  for(int iWord=0; iWord<request_nof_words; iWord++) {
    DEBUG("Word : "<<std::setw(5)<<std::dec<<iWord<<"  0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<gl_dma_buffer[iWord]);
    raw_payload[iWord]=gl_dma_buffer[iWord];
  }

  return 0;
}

int vx1730::DumpFrontEvent( std::string outputfile, DumpMode mode, bool debug ){
  DEBUG("vx1730::DumpFrontEvent");

  // FIXME : these are variables that should be controlled by DAQ
  uint32_t source_id = 0;
  uint64_t event_id  = 0;

  /////////////////////////////////
  // get the event
  /////////////////////////////////
  DEBUG("Get the Event");
  uint32_t raw_payload[MAXFRAGSIZE];
  ReadRawEvent( raw_payload, debug );

  if(debug){
    DEBUG("Print the header manually for debugging");
    for(int iWord=0; iWord<4; iWord++){
      DEBUG("Word : "<<iWord<<"  "<<raw_payload[iWord]);
    }
  }
  
  if(debug){
    DEBUG("Dumping payload to check");
    Payload_Dump( raw_payload );
  }

  DumpEventCount( debug );

  /////////////////////////////////
  // format the event by putting the FASER header on it
  /////////////////////////////////
  DEBUG("Format the event by putting the FASER header on it");
  int payload_size = Payload_GetEventSize( raw_payload );

  if(debug){
    DEBUG("Raw Payload size           : "<<std::dec<<payload_size);
  }

  // event fragment creation
  std::unique_ptr<EventFragment> data(new EventFragment(0, source_id, event_id, 0xFFFF, raw_payload,payload_size * 4));

  /////////////////////////////////
  // check that it copied by dumping again
  /////////////////////////////////
  DEBUG("Check that it copied by dumping again");
  if(debug)
    Data_Dump( data, 5, debug );

  Data_Write( data, outputfile, mode);

  return 0;
}

int vx1730::GetBufferLength( unsigned int code ){
  DEBUG("vx1730::GetBufferLength");

  int n_samples_per_buffer = 0;

  if(code==0x0)
    n_samples_per_buffer = 639990;
  else if(code==0x1)
    n_samples_per_buffer = 319990;
  else if(code==0x2)
    n_samples_per_buffer = 159990;
  else if(code==0x3)
    n_samples_per_buffer = 79990;
  else if(code==0x4)
    n_samples_per_buffer = 39990;
  else if(code==0x5)
    n_samples_per_buffer = 19990;
  else if(code==0x6)
    n_samples_per_buffer = 9990;
  else if(code==0x7)
    n_samples_per_buffer = 4990;
  else if(code==0x8)
    n_samples_per_buffer = 2550;
  else if(code==0x9)
    n_samples_per_buffer = 1270;
  else if(code==0xA)
    n_samples_per_buffer = 630;

  return n_samples_per_buffer;
}
