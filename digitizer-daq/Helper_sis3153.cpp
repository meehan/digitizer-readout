//============================================================================
// Author : Sam Meehan <samuel.meehan@cern.ch>
//
// Description : Lowest level functionality for reading the master and slave VME boards
//
// Relevant documentation :
//  - https://faserdaq.web.cern.ch/faserdaq/sis3153-m-usb-1-v106-manual.pdf
//  - https://faserdaq.web.cern.ch/faserdaq/sis3153-m-eth-1-v107-ethernet-addendum.pdf
//
// Error codes from communication :
//   0x111:no Ethernet connection
//   0x211:VME Buss Error
//   0x212:VME Retry
//   0x214:VME Arbitration Timeout
//============================================================================

#include "Helper_sis3153.h"

void ReadMasterReg( sis3153eth* crate, unsigned int addr, unsigned int& data, bool debug){
  int return_code;
  unsigned int data_read; // temporary variable that is then copied to the input reference

  return_code = crate->udp_sis3153_register_read (addr, &data_read);
  data = data_read;

  DEBUG("ReadMasterReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);
  
  if(return_code!=0){
    ERROR("ReadMasterReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);
  
    if(return_code==111)
      ERROR("No Ethernet Connection - Check the IP address, or plugging or ethernet cables.");
    if(return_code==211)
      ERROR("VME Bus Error - Is the board fully plugged in. Remember, the VME crate in b161 is finicky.");
    if(return_code==212)
      ERROR("VME Retry");
    if(return_code==214)
      ERROR("MVE Arbitration Timeout");
    
    THROW(DigitizerHardwareException, "Unable to perform ReadMasterReg at addr = "+std::to_string(addr)+"    return_code = "+std::to_string(return_code));
  }
}

void WriteMasterReg( sis3153eth* crate, unsigned int addr, unsigned int data, bool debug){
  int return_code;
  return_code = crate->udp_sis3153_register_write (addr, data);
  
  DEBUG("ReadMasterReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);

  if(return_code!=0){
    ERROR("WriteMasterReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);
  
    if(return_code==111)
      ERROR("No Ethernet Connection - Check the IP address, or plugging or ethernet cables.");
    if(return_code==211)
      ERROR("VME Bus Error - Is the board fully plugged in. Remember, the VME crate in b161 is finicky.");
    if(return_code==212)
      ERROR("VME Retry");
    if(return_code==214)
      ERROR("MVE Arbitration Timeout");
      
    THROW(DigitizerHardwareException, "Unable to perform WriteMasterReg at addr = "+std::to_string(addr)+"    return_code = "+std::to_string(return_code));
  }
}

void ReadSlaveReg( sis3153eth* crate, unsigned int addr, unsigned int& data, bool debug){
  int return_code;
  unsigned int data_read;

  return_code = crate->vme_A32D32_read (addr, &data_read);
  data = data_read;
  
  DEBUG("ReadSlaveReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);

  if(return_code!=0){
    ERROR("ReadSlaveReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);
    
    if(return_code==111)
      ERROR("No Ethernet Connection - Check the IP address, or plugging or ethernet cables.");
    if(return_code==211)
      ERROR("VME Bus Error - Is the board fully plugged in. Remember, the VME crate in b161 is finicky.");
    if(return_code==212)
      ERROR("VME Retry");
    if(return_code==214)
      ERROR("MVE Arbitration Timeout");
    
    THROW(DigitizerHardwareException, "Unable to perform ReadSlaveReg at addr = "+std::to_string(addr)+"    return_code = "+std::to_string(return_code));
  }
}

void WriteSlaveReg( sis3153eth* crate, unsigned int addr, unsigned int data, bool debug){
  int return_code;
  return_code = crate->vme_A32D32_write (addr, data);

  DEBUG("WriteSlaveReg :  addr = "<<std::hex<<addr<<"    data = "<<std::hex<<data<<"    return_code = "<<std::dec<<return_code);

  if(return_code!=0){
    ERROR("WriteSlaveReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);
    
    if(return_code==111)
      ERROR("No Ethernet Connection - Check the IP address, or plugging or ethernet cables.");
    if(return_code==211)
      ERROR("VME Bus Error - Is the board fully plugged in. Remember, the VME crate in b161 is finicky.");
    if(return_code==212)
      ERROR("VME Retry");
    if(return_code==214)
      ERROR("MVE Arbitration Timeout");
    
    THROW(DigitizerHardwareException, "Unable to perform WriteSlaveReg at addr = "+std::to_string(addr)+"    return_code = "+std::to_string(return_code));
  }
}

int sis3153_TestComm( sis3153eth* crate, bool debug ){
  INFO("sis3153_TestComm");

  int return_code;
  UINT data;

  // read the basic configuration information of the interface board
  ReadMasterReg(crate, SIS3153_CONTROL_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_MODID_VERSION, data, debug);
  ReadMasterReg(crate, SIS3153_SERIAL_NUMBER_REG, data, debug);
  ReadMasterReg(crate, SIS3153_LEMO_IO_CTRL_REG, data, debug);
  ReadMasterReg(crate, SIS3153_VME_MASTER_CONTROL_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_VME_MASTER_CYCLE_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_VME_INTERRUPT_STATUS, data, debug);

  // turn the LED A on and off to demonstrate functionality of communication
  DEBUG("=====================================");
  DEBUG("Testing interface card communications with LED A on and off");
  DEBUG("=====================================");
  DEBUG("Turn LED-A on");
  
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x1, debug);
  usleep(100000);
  
  DEBUG("Turn LED-A off");
  
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x10000, debug);
  usleep(100000);
  
  DEBUG("Turn LED-A on");
  
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x1, debug);
  
  DEBUG("The light should still be on for LED A on the interface card");

  // this block is only present to toggle the system reset bit
  if(false){
    // if Switch SW162-7 is ON and VME_SYSRESET bit is set then VME_SYSRESET is issued
    INFO("SysReset");
  
    UINT data_sys_reset;

    ReadMasterReg(crate, SIS3153_VME_MASTER_CONTROL_STATUS, data_sys_reset, debug);

    DEBUG("Data System Reset Initial : "<<ConvertIntToWord(data_sys_reset));

    SetBit(data_sys_reset, 16, 0);
    SetBit(data_sys_reset, 0, 1);

    DEBUG("Data System Reset Initial : "<<ConvertIntToWord(data_sys_reset));

    //WriteMasterReg(crate, SIS3153_VME_MASTER_CONTROL_STATUS, data_sys_reset, debug);
  }

  return 0;
}


