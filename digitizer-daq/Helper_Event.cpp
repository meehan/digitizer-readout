//============================================================================
// Author : Sam Meehan <samuel.meehan@cern.ch>
//
// Description : Helper functions for dealing with unpacking the payload from the digitizer board
//
// Relevant documentation :
//  - http://faserdaq.web.cern.ch/faserdaq/UM2792_V1730_V1725_rev2.pdf
//  - https://faserdaq.web.cern.ch/faserdaq/UM5118_725-730__Registers_Description_rev2.pdf
//
//============================================================================

#include "Helper_Event.h"

int Payload_GetEventSize( const uint32_t raw_payload[], bool debug ){
  INFO("\n\n[] Payload_GetEventSize");

  // TODO : This assumes it was a successful read with a good header

  unsigned int eSize = raw_payload[0] & 0x0FFFFFF;

  INFO("EventSize : "<<std::dec<<eSize);

  return eSize;
}

int Payload_GetChannelMask( const uint32_t raw_payload[], std::vector< bool >& channel_mask, bool debug){
  INFO("\n\n[] Payload_GetChannelMask");

  channel_mask.clear();

  // assume a channel is not enabled
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    channel_mask.push_back(false);
  }

  // save the channel masks
  // TODO : Make the number of channels not hardcoded
  for(int iChan=0; iChan<8; iChan++){
    channel_mask.at(iChan) = (raw_payload[1] & (1<<iChan)) >> iChan;
  }

  for(int iChan=8; iChan<16; iChan++){
    channel_mask.at(iChan) = (raw_payload[2] & (1<<(16+iChan))) >> (16+iChan);
  }

  // print out the channel mask in debug mode
  if(debug){
    for(int iChan=0; iChan<NCHANNELS; iChan++){
      INFO("ChannelMask : "<<channel_mask.at(iChan));
    }
  }

  return 0;
}

int Payload_GetBufferLength( const uint32_t raw_payload[], int& words_per_channel, int& samples_per_channel, bool debug){
  INFO("\n\n[] Payload_GetBufferLength");

  // get channel mask
  std::vector<bool> temp_channel_mask;
  Payload_GetChannelMask(raw_payload, temp_channel_mask, true);

  // find number of summed channels
  int n_channel_active=0;
  for(int iChan=0; iChan<temp_channel_mask.size(); iChan++){
    if(temp_channel_mask.at(iChan)==true)
      n_channel_active++;
  }

  // get event size
  int eSize=Payload_GetEventSize( raw_payload );

  // subtract 4 for the header
  int eSizeMod = eSize-4;

  // divide modified event size by number of channels
  words_per_channel = eSizeMod/n_channel_active;

  samples_per_channel = 2*words_per_channel;

  return 0;
}

int Payload_Dump( const uint32_t raw_payload[], bool debug ){
  INFO("\n\n[] Payload_Dump");

  unsigned int EventSize = Payload_GetEventSize( raw_payload, debug );
  
  INFO("Payload_Dump EventSize : "<<EventSize);

  INFO("DUMP - Header");
  for(int iWord=0; iWord<4; iWord++){
    INFO("head "<<std::dec<<iWord<<" : "<<std::setfill('0')<<std::setw(8)<<std::hex<<raw_payload[iWord]);
  }

  INFO("DUMP - Data");
  for(int iWord=4; iWord<EventSize; iWord++){
    INFO("data "<<std::dec<<iWord<<" : "<<std::setfill('0')<<std::setw(8)<<std::hex<<raw_payload[iWord]);
  }

  return 0;
}

int Payload_Parse(const  uint32_t raw_payload[], std::vector< std::vector< unsigned int >>& data_parsed, bool debug){
  INFO("\n\n[] Payload_Parse");

  // clear the data that was in the output
  data_parsed.clear();

  int eSize = Payload_GetEventSize(raw_payload);

  std::vector<bool> channel_mask;
  Payload_GetChannelMask(raw_payload, channel_mask, true);

  int words_per_channel;
  int samples_per_channel;
  Payload_GetBufferLength(raw_payload, words_per_channel, samples_per_channel, true);

  INFO("Samples : "<<samples_per_channel<<"  "<<words_per_channel);

  // TODO - put some checks that the number of channels and the buffer length makes sense

  // now parse the event data buffer depending on the channel mask info
  unsigned int currentLoc=4;

  for(int iChan=0; iChan<NCHANNELS; iChan++){

    INFO("Location : "<<iChan<<" "<<currentLoc);

    data_parsed.push_back({});

    if(channel_mask.at(iChan)==1){
      INFO("Channel good to read : "<<iChan<<" start = "<<currentLoc);
      for(int iDat=currentLoc; iDat<currentLoc+words_per_channel; iDat++){

        // two readings are stored in one word
        unsigned int chData = raw_payload[iDat];

        unsigned int ev0    = (chData & 0xFFFF0000) >> 16;  // first half of event doublet
        unsigned int ev1    = (chData & 0x0000FFFF);        // second half of event doublet

        data_parsed.at(iChan).push_back( ev1 );
        data_parsed.at(iChan).push_back( ev0 );

      }

      // move where we are looking
      currentLoc += words_per_channel;
    }

  }



  return 0;
}



int Data_Dump( std::unique_ptr<EventFragment>& data, int n_dump, bool debug){
  INFO("\n\n[] Data_Dump");

  INFO("\nHeader(FASER)");

  INFO(" fragment_tag   - "<<std::hex<<unsigned(data->fragment_tag())   );
  INFO(" trigger_bits   - "<<std::hex<<data->trigger_bits()   );
  INFO(" payload_size   - "<<std::hex<<data->payload_size()   );
  INFO(" total size     - "<<std::hex<<data->size()           );
  INFO(" source_id      - "<<std::hex<<data->source_id()      );
  INFO(" event_id       - "<<std::hex<<data->event_id()       );
  INFO(" bc_id          - "<<std::hex<<data->bc_id()          );
  INFO(" status         - "<<std::hex<<data->status()         );
  INFO(" timestamp      - "<<std::hex<<data->timestamp()      );

  INFO("\nData Payload - Header");
  const uint32_t *payload=data->payload<const uint32_t*>();
  
  for(int iPay=0; iPay<4; iPay++){
    INFO(" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(payload[iPay]));
  }

  INFO("\nData Payload - Data ["<<n_dump<<" words] Beginning");
  for(int iPay=4; iPay<4+n_dump; iPay++){
    INFO(" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(payload[iPay]));
  }

  INFO("\nData Payload - Data ["<<n_dump<<" words] End");
  for(int iPay=data->payload_size()-n_dump; iPay<data->payload_size(); iPay++){
    INFO(" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(payload[iPay]));
  }

  return 0;
}


int Data_Write( std::unique_ptr<EventFragment>& data, std::string outputpath, DumpMode mode, bool debug){
  INFO("\n\n[] Data_Write : "<<outputpath);

  std::ofstream fout;
  
  if(mode = DumpMode::New){
    INFO("Making new file : "<<outputpath);
    fout.open(outputpath.c_str(), std::ios::out);
  }
  else if(mode = DumpMode::Append){
    INFO("Appending to file : "<<outputpath);
    fout.open(outputpath.c_str(), std::ios::app);
  }
  else{
    INFO("This is not a valid write method");
  }
  
  fout<<std::endl<<"<event>"<<std::endl;

  if(debug) INFO("\nHeader(FASER)");
  //  fout<<"faser-marker         - "<<std::hex<<unsigned(data->header.marker)         <<std::endl;
  fout<<"faser-fragment_tag   - "<<std::hex<<unsigned(data->fragment_tag())   <<std::endl;
  fout<<"faser-trigger_bits   - "<<std::hex<<data->trigger_bits()             <<std::endl;
  //  fout<<"faser-version_number - "<<std::hex<<data->header.version_number           <<std::endl;
  //  fout<<"faser-header_size    - "<<std::hex<<data->header.header_size              <<std::endl;
  fout<<"faser-payload_size   - "<<std::hex<<data->payload_size()             <<std::endl;
  fout<<"faser-source_id      - "<<std::hex<<data->source_id()                <<std::endl;
  fout<<"faser-event_id       - "<<std::hex<<data->event_id()                 <<std::endl;
  fout<<"faser-bc_id          - "<<std::hex<<data->bc_id()                    <<std::endl;
  fout<<"faser-status         - "<<std::hex<<data->status()                   <<std::endl;
  fout<<"faser-timestamp      - "<<std::hex<<data->timestamp()                <<std::endl;

  const uint32_t *payload=data->payload<const uint32_t* >();
  if(debug) INFO("\nData Payload - Header");
  fout<<"payload-header0 : "<<std::hex<<unsigned(payload[0])<<std::endl;
  fout<<"payload-header1 : "<<std::hex<<unsigned(payload[1])<<std::endl;
  fout<<"payload-header2 : "<<std::hex<<unsigned(payload[2])<<std::endl;
  fout<<"payload-header3 : "<<std::hex<<unsigned(payload[3])<<std::endl;


  std::vector< std::vector< unsigned int >> parsed_data;
  Payload_Parse(payload, parsed_data );

  std::vector<bool> channel_mask;
  Payload_GetChannelMask(payload, channel_mask, true);

  int words_per_channel;
  int samples_per_channel;
  Payload_GetBufferLength(payload, words_per_channel, samples_per_channel, true);

  if(debug)
    INFO("Data Payload");

  // reading value
  fout<<"payload-data "<<std::setw(10)<<"reading";
  // write data for each channel that is active and -1 if its not active
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    std::string line = "chan-"+std::to_string(iChan);
    fout<<std::setw(13)<<line;
  }
  fout<<"\n";

  for(int iData=0; iData<samples_per_channel; iData++){

    if(debug)
      INFO("Writing out : "<<iData);

    // reading value
    fout<<"PAYLOAD-DATA "<<std::setw(10)<<std::dec<<iData<<"   ";

    // write data for each channel that is active and -1 if its not active
    for(int iChan=0; iChan<NCHANNELS; iChan++){
      if(channel_mask.at(iChan)==1){
        fout<<std::setw(10)<<parsed_data.at(iChan).at(iData)<<"   ";
      }
      else{
        fout<<std::setw(10)<<"-1"<<"   ";
      }
    }

    // write a newline
    fout<<"\n";

  }

  fout.close();

  return 0;
}
