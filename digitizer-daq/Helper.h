//============================================================================
// Name        : DigitizerHelper
// Author      : Sam Meehan
// Version     : v0.0
// Copyright   : Your copyright notice
// Description : Helper functions for the FASER digitizer readout
//============================================================================

#ifndef  HELPER_INCLUDE_H
#define  HELPER_INCLUDE_H

#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cmath>

#include <signal.h>

#include <ctime>
#include <chrono>
using namespace std::chrono;

#include "nlohmann/json.hpp"
// for convenience
using json = nlohmann::json;



// only needed for standalone compilation, otherwise you want to use the real logging in daqling
#include "Logging.hpp"

// for exception construction
#include "Exceptions/Exceptions.hpp"

class DigitizerHardwareException : public Exceptions::BaseException { using Exceptions::BaseException::BaseException; };



void printHelp();

json openJsonFile(std::string filepath);

std::string ConvertIntToWord(unsigned int word, bool debug=false);

void SetBit(unsigned int& word, int bit_location, int bit_val);

int GetBit(unsigned int word, int bit_location);

std::string GetDateVerboseString();

void Wait(float nseconds);


#endif




