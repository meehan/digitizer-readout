//============================================================================
// Name        : sis3153eth_access_test.cpp
// Author      : th
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <sstream>

#include "Registers_sis3153.h"
#include "Registers_v1720.h"
#include "Registers_vx1730.h"

#include "DigitizerHelper.h"

using namespace std;

#include "nlohmann/json.hpp"
// for convenience
using json = nlohmann::json;




int main(int argc, char *argv[])
{

  // require that you point the program at a config file
  if(argc<=1){
    std::cout<<"You need to provide a config file input"<<std::endl;
    return 0;
  }

  // argument parsing to get the path to the config file
  char *cPath = NULL;
  int index;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "hc:")) != -1)
    switch (c)
      {
      case 'h':
        printHelp();
        break;
      case 'c':
        cPath = optarg;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument. Path to the config file.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort();
      }


  // open the json config file
  std::string configPath(cPath);
  std::cout<<"Config Path : "<<configPath<<std::endl;
  json myConfig = openJsonFile(configPath.c_str());

  // ip address
  char  ip_addr_string[32] ;
  strcpy(ip_addr_string, std::string(myConfig["ip"]).c_str() ) ; // SIS3153 IP address
  std::cout<<"IP Address : "<<ip_addr_string<<std::endl;

  // vme base address
  std::string vme_base_address_str = std::string(myConfig["vme_base_address"]);
  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  std::cout<<"Base VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address<<std::endl;

  //channel enable
  std::vector< json > channelConfig = myConfig["channel_config"];
  std::cout<<"NChannels : "<<channelConfig.size()<<std::endl;
  for(int iChan=0; iChan<channelConfig.size(); iChan++){
    std::cout<<"channelConfig : channel = "<<setw(10)<<channelConfig.at(iChan)["channel"]<<" | enable = "<<setw(10)<<channelConfig.at(iChan)["enable"]<<" | dc_offset = "<<setw(10)<<channelConfig.at(iChan)["dc_offset"]<<std::endl;
  }






  unsigned int addr;
  unsigned int data ;
  unsigned int i ;
  unsigned int request_nof_words ;
  unsigned int got_nof_words ;
  unsigned int written_nof_words ;

  unsigned char uchar_data  ;
  unsigned short ushort_data  ;

  char ch_string[64] ;
  int int_ch ;
  int return_code ;


  /////////////////////////////////
  // open the vme crate connection
  /////////////////////////////////
  sis3153eth *vme_crate;
  sis3153eth(&vme_crate, ip_addr_string);

  char char_messages[128] ;
  unsigned int nof_found_devices ;

  return_code = vme_crate->vmeopen();  // open Vme interface
  vme_crate->get_vmeopen_messages(char_messages, &nof_found_devices);  // open Vme interface
  printf("get_vmeopen_messages = %s , nof_found_devices %d \n",char_messages, nof_found_devices);


  /////////////////////////////////
  // test interface board sis3153 communications
  /////////////////////////////////
  sis3153_TestComm( vme_crate );

  /////////////////////////////////
  // test digitizer board v1720 communications
  /////////////////////////////////
  v1720_TestComm( vme_crate, vme_base_address );

  /////////////////////////////////
  // configure v1720
  /////////////////////////////////
  v1720_Configure( vme_crate, vme_base_address, myConfig);

  int BufferLength = GetBufferLength( vme_crate, vme_base_address );

  std::cout<<"BufferLength = "<<BufferLength<<std::endl;

  /////////////////////////////////
  // START data acquisition
  /////////////////////////////////
  v1720_StartAcquisition( vme_crate, vme_base_address );



  /////////////////////////////////
  // trigger and readout
  /////////////////////////////////
  std::cout<<"\n\n[] Pre-Trigger Info"<<std::endl;
  std::cout<<"NEvents"<<std::endl;
  addr = vme_base_address + V1720_EVENT_STORED;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Event Size"<<std::endl;
  addr = vme_base_address + V1720_EVENT_SIZE;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status"<<std::endl;
  addr = vme_base_address + V1720_VME_STATUS;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status Acquisition"<<std::endl;
  addr = vme_base_address + V1720_ACQUISITION_STATUS;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  usleep(100000);

  std::cout<<"\n\n==========================="<<std::endl;
  std::cout<<"Sending triggers"<<std::endl;
  std::cout<<"==========================="<<std::endl;
  UINT nEvents=0;
  UINT thisEventSize=0;
  UINT readoutStatus;
  UINT acquisitionStatus;

  int x_in;
  std::cout<<"Waiting for input - press and button"<<std::endl;
  std::cin>>x_in;


  SendSoftwareTrigger( vme_crate, vme_base_address, nEvents, thisEventSize, readoutStatus, acquisitionStatus);



  // manual read using single reads
  if(false){

    ////////////////////////////////////////////////////////////////////////////////////
    // read header
    ////////////////////////////////////////////////////////////////////////////////////
    std::vector< UINT > channelMask;
    UINT eventSize;
    GetHeader(vme_crate, vme_base_address, channelMask, eventSize);

    ////////////////////////////////////////////////////////////////////////////////////
    // read data
    ////////////////////////////////////////////////////////////////////////////////////
    UINT nChan=8;
    std::vector< std::vector< UINT > > parsedChannelData;
    GetEventData( vme_crate, vme_base_address, parsedChannelData, channelMask, eventSize, BufferLength, nChan );

    ////////////////////////////////////////////////////////////////////////////////////
    // write data to output file
    ////////////////////////////////////////////////////////////////////////////////////

    std::cout<<"\nChecking Channel Data"<<std::endl;
    for(int iChan=0; iChan<nChan; iChan++){
      std::cout<<"Channel : "<<iChan<<"  -  Size : "<<parsedChannelData.at(iChan).size()<<std::endl;
    }

  }


  if(true){

    // get the number of events
    std::cout<<"NEvents"<<std::endl;
    UINT data_nevents;
    addr = vme_base_address + V1720_EVENT_STORED;
    return_code = vme_crate->vme_A32D32_read (addr, &data_nevents); //
    printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data_nevents, return_code);

    // get the event size
    std::cout<<"Event Size"<<std::endl;
    UINT data_eventsize;
    addr = vme_base_address + V1720_EVENT_SIZE;
    return_code = vme_crate->vme_A32D32_read (addr, &data_eventsize); //
    printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data_eventsize, return_code);

    std::cout<<"NEvents : "<<std::dec<<data_nevents<<std::endl;
    std::cout<<"EventSize(hex) : "<<std::hex<<data_eventsize<<std::endl;
    std::cout<<"EventSize(dec) : "<<std::dec<<data_eventsize<<std::endl;

    // read out event in its entirety using block read

    uint32_t gl_dma_buffer[MAXFRAGSIZE] ;
    unsigned int request_nof_words = data_eventsize;
    unsigned int got_nof_words=-1;

    for(int iWord=0; iWord<request_nof_words; iWord++) {
      gl_dma_buffer[iWord] = 0 ;
    }
    addr = vme_base_address + 0x0000 ;
    return_code = vme_crate->vme_A32BLT32_read (addr, gl_dma_buffer, request_nof_words, &got_nof_words); //
    printf("vme_A32BLT32_read:  \t\taddr = 0x%08X    \tgot_nof_words = 0x%08X \treturn_code = 0x%08X \n", addr, got_nof_words,return_code);

    // TODO : put a check that the number of requested words is the same as the number of gotten words

    for(int iWord=0; iWord<request_nof_words; iWord++) {
      std::cout<<"Word : "<<std::setw(5)<<std::dec<<iWord<<"  0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<gl_dma_buffer[iWord]<<std::endl;
    }

    // header
    std::cout<<"Header Read"<<std::endl;
    for(int iWord=0; iWord<(int)4; iWord++){
      printf("Header:  \t\t = 0x%08X\n", gl_dma_buffer[iWord]);
    }

    // TODO : put some checks and printout for the header info
    // are the first few bits 0101?

    // get the event size for this read
    // this includes the 4 bytes for the header so we remove them
    UINT event_size = (gl_dma_buffer[0] & 0x0FFFFFF); // recover the last bits that are the event size using a mask

    // save the channel masks
    std::vector< UINT > channel_mask;
    channel_mask.push_back( (gl_dma_buffer[1] & 0x01) >> 0 );
    channel_mask.push_back( (gl_dma_buffer[1] & 0x02) >> 1 );
    channel_mask.push_back( (gl_dma_buffer[1] & 0x04) >> 2 );
    channel_mask.push_back( (gl_dma_buffer[1] & 0x08) >> 3 );
    channel_mask.push_back( (gl_dma_buffer[1] & 0x10) >> 4 );
    channel_mask.push_back( (gl_dma_buffer[1] & 0x20) >> 5 );
    channel_mask.push_back( (gl_dma_buffer[1] & 0x40) >> 6 );
    channel_mask.push_back( (gl_dma_buffer[1] & 0x80) >> 7 );

    // data read
    std::cout<<"Header Read"<<std::endl;
    for(int iWord=4; iWord<event_size; iWord++){
      UINT ev0    = (gl_dma_buffer[iWord] & 0xFFFF0000) >> 16;  // first half of event doublet
      UINT ev1    = (gl_dma_buffer[iWord] & 0x0000FFFF);        // second half of event doublet

      std::cout<<std::dec<<"iWord(0) : "<<ev0<<std::endl;
      std::cout<<std::dec<<"iWord(1) : "<<ev1<<std::endl;

    }





    // packing into the right space

    int payload_size = data_eventsize;

    const int total_size = sizeof(EventFragmentHeader) + sizeof(char) * payload_size;

    std::cout<<"total_size : "<<total_size<<std::endl;
    std::cout<<"payload_size : "<<payload_size<<std::endl;

    // create the EventFragmet object by allocating the proper amount of memory
    std::unique_ptr<EventFragment> data((EventFragment *)malloc(total_size));

    // fill in *our* header
    data->header.marker         = 0x03;
    data->header.fragment_tag   = 0x18;
    data->header.trigger_bits   = 0x0101;
    data->header.version_number = 0x0001;
    data->header.header_size    = 0x0020;
    data->header.payload_size   = 0x01020304;
    data->header.payload_size   = 0x01020304;
    data->header.source_id      = 0x01020304;
    data->header.event_id       = 0x0102030405060708;
    data->header.bc_id          = 0x0104;
    data->header.status         = 0x0102;
    data->header.timestamp      = 0x0102030405060708;



    std::cout<<"Header : marker         - "<<std::hex<<unsigned(data->header.marker)         <<std::endl;
    std::cout<<"Header : fragment_tag   - "<<std::hex<<unsigned(data->header.fragment_tag)   <<std::endl;
    std::cout<<"Header : trigger_bits   - "<<std::hex<<data->header.trigger_bits   <<std::endl;
    std::cout<<"Header : version_number - "<<std::hex<<data->header.version_number <<std::endl;
    std::cout<<"Header : header_size    - "<<std::hex<<data->header.header_size    <<std::endl;
    std::cout<<"Header : payload_size   - "<<std::hex<<data->header.payload_size   <<std::endl;
    std::cout<<"Header : payload_size   - "<<std::hex<<data->header.payload_size   <<std::endl;
    std::cout<<"Header : source_id      - "<<std::hex<<data->header.source_id      <<std::endl;
    std::cout<<"Header : event_id       - "<<std::hex<<data->header.event_id       <<std::endl;
    std::cout<<"Header : bc_id          - "<<std::hex<<data->header.bc_id          <<std::endl;
    std::cout<<"Header : status         - "<<std::hex<<data->header.status         <<std::endl;
    std::cout<<"Header : timestamp      - "<<std::hex<<data->header.timestamp      <<std::endl;


    // decompose the payload into a character array
    std::cout<<"Payload allocation : "<<payload_size<<std::endl;
    char * payload_char;
    payload_char = (char*)malloc(payload_size*4 + 1); // need to allocate 4 times the space since each word in the payload is 32 bits

    std::cout<<"payload_char Size : "<<sizeof(payload_char)<<std::endl;


    for(int iWord=0; iWord<4; iWord++){

      uint8_t pl0 = (gl_dma_buffer[iWord] & 0xFF000000) >> 24;
      uint8_t pl1 = (gl_dma_buffer[iWord] & 0x00FF0000) >> 16;
      uint8_t pl2 = (gl_dma_buffer[iWord] & 0x0000FF00) >> 8;
      uint8_t pl3 = (gl_dma_buffer[iWord] & 0x000000FF);

      std::cout<<"iWord : "<<std::dec<<iWord<<std::endl;
      std::cout<<std::setfill('0')<<std::setw(8)<<std::hex<<gl_dma_buffer[iWord]<<std::endl;
      std::cout<<std::setfill('0')<<std::setw(2)<<std::hex<<unsigned(pl0)<<std::endl;
      std::cout<<std::setfill('0')<<std::setw(2)<<std::hex<<unsigned(pl1)<<std::endl;
      std::cout<<std::setfill('0')<<std::setw(2)<<std::hex<<unsigned(pl2)<<std::endl;
      std::cout<<std::setfill('0')<<std::setw(2)<<std::hex<<unsigned(pl3)<<std::endl;

    }



    // copy
    memcpy(data->payload, gl_dma_buffer, 16);

    std::cout<<"PayloadSize : "<<sizeof(data->payload)<<std::endl;

    std::cout<<"Header"<<std::endl;
    for(int iPay=0; iPay<4; iPay++){
      std::cout<<"header check : "<<std::dec<<iPay<<"  "<<std::hex<<gl_dma_buffer[iPay]<<std::endl;
    }

    std::cout<<"Payload Header"<<std::endl;
    for(int iPay=0; iPay<16; iPay++){
      std::cout<<"--"<<std::endl;
      std::cout<<"payload check (norm)        : "<<std::dec<<iPay<<"  "<<std::hex<<data->payload[iPay]<<std::endl;
      std::cout<<"payload check (unsigned)    : "<<std::dec<<iPay<<"  "<<std::hex<<unsigned(data->payload[iPay])<<std::endl;
      std::cout<<"payload check (int)         : "<<std::dec<<iPay<<"  "<<std::hex<<int(data->payload[iPay])<<std::endl;
      std::cout<<"payload check (uint8_t)     : "<<std::dec<<iPay<<"  "<<std::hex<<unsigned(uint8_t(data->payload[iPay]))<<std::endl;
    }




  }



  //WriteOutput(parsedChannelData, channelMask, BufferLength, nChan, "testOutput.txt");


  std::cout<<"\n\n[] Post-ReadOut Info"<<std::endl;
  std::cout<<"NEvents"<<std::endl;
  addr = vme_base_address + V1720_EVENT_STORED;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Event Size"<<std::endl;
  addr = vme_base_address + V1720_EVENT_SIZE;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status"<<std::endl;
  addr = vme_base_address + V1720_VME_STATUS;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);

  std::cout<<"Status Acquisition"<<std::endl;
  addr = vme_base_address + V1720_ACQUISITION_STATUS;
  return_code = vme_crate->vme_A32D32_read (addr, &data); //
  printf("vme_A32D32_read:  \t\taddr = 0x%08X    \tdata = 0x%02X    \treturn_code = 0x%08X \n", addr, data,return_code);





  /////////////////////////////////
  // STOP data acquisition
  /////////////////////////////////
  v1720_StopAcquisition( vme_crate, vme_base_address );














  return 0;
}































