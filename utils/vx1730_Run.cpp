//============================================================================
// Name        : vx1730_Run
// Author      : Sam Meehan
// Description : Executable for developing the DAQ readout of vx1730
//============================================================================

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"

#include "Comm_vx1730.h"

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <signal.h>
#include <getopt.h>

// this searches for an exit via 
void my_handler(int s){
  printf("********************************\n");
  printf("STOP It!!!! : Caught signal %d\n",s);
  printf("********************************\n");
  exit(1); 
}

// for argument parsing
#define no_argument 0
#define required_argument 1 
#define optional_argument 2

int main(int argc, char *argv[])
{

  // for catching the exit 
  signal(SIGINT, my_handler);

  // used to tag the name of the program if you want in the output file
  std::string programName = argv[0];
  std::cout<<"Program Running : "<<programName<<std::endl;
  std::size_t pos = programName.find("/");
  std::string programTag = programName.substr(pos+1);
  std::cout<<"Program Tag     : "<<programTag<<std::endl;

  // require that you point the program at a config file
  // which will at least be used for the ip address and local base address
  if(argc<=1){
    std::cout<<"********************************************"<<std::endl;
    std::cout<<"Incorrect Usage : Need at least a --runtype argument to "<<std::endl;
    std::cout<<"to tell the program which functionality to execute"<<std::endl;
    std::cout<<"Please check usage via the [-h] help option"<<std::endl;
    std::cout<<"********************************************"<<std::endl;
    return 1;
  }
  
  // get optional arguments from parser
  // https://www.geeksforgeeks.org/getopt-function-in-c-to-parse-command-line-arguments/
  // https://stackoverflow.com/questions/8793020/using-getopt-long-c-how-do-i-code-up-a-long-short-option-to-both-require-a
  
  const struct option longopts[] =
  {
    {"help",    no_argument, 0, 'h'},
    {"runtype", required_argument, 0, 'r'},
    {"config",  required_argument, 0, 'c'},
    {"output",  required_argument, 0, 'o'},
    {"force",   no_argument, 0, 'f'},
    {"debug",   no_argument, 0, 'd'},
    {"nevents", required_argument, 0, 'n'},
    {0,0,0,0}
  };
  
  int index;
  int iarg=0;
  char *rType = NULL;
  char *cPath = NULL;
  char *oPath = NULL;
  bool forceRemove = false;
  bool debug = false;
  int  nAcquire = -1;
  
  while((iarg = getopt_long(argc, argv, ":h:r:c:o:f:d", longopts, &index)) != -1)
  {
    switch (iarg)
    {
      case 'h':
        std::cout<<std::endl<<std::endl;
        std::cout<<"This is the help screen for your vx1730 digitizer command line running"<<std::endl<<std::endl;
        std::cout<<"Synopsis : ./vx1730_Run -r --runtype [RUNTYPE]"<<std::endl;
        std::cout<<"Optional arguments : "<<std::endl;
        std::cout<<" -c --config    [CONFIGFILE] : CONFIGFILE is the path to the json formatted config file"<<std::endl;
        std::cout<<" -o --output    [OUTPUTPATH] : OUTPUTPATH is the location of the output file"<<std::endl;
        std::cout<<" -f --force                  : If set, this will remove the output file"<<std::endl;
        std::cout<<" -d --debug                  : If set, this will cause debug flag to be set which is used throughout"<<std::endl;
        std::cout<<" -n --nevents   [NEVENTS]    : NEVENTS is the number of events that will be triggered before dumping the data and exitting"<<std::endl;
        
        std::cout<<"Example : How to ping the module to check its connectivity"<<std::endl;
        std::cout<<"          cmd-prompt> ./vx1730_Run --runtype showconfig --config ../configs/cfg_vx1730.json"<<std::endl;
        std::cout<<std::endl;
        return 1;
        break;
      case 'r':
        std::cout << "Argument : Run type = " << optarg << std::endl;
        rType = optarg;
        break;
      case 'c':
        std::cout << "Argument : Config file = " << optarg << std::endl;
        cPath = optarg;
        break;
      case 'o':
        std::cout << "Argument : Output file = " << optarg << std::endl;
        oPath = optarg;
        break;
      case 'f':
        std::cout << "Argument : Force remove" << std::endl;
        forceRemove=true;
        break;
      case 'd':
        std::cout << "Argument : Debug flag set" << std::endl;
        debug = true;
        break;
      case 'n':
        std::cout << "Argument : NEvents to acquire" << optarg << std::endl;
        nAcquire = std::stoi(optarg);
        break;
      case '?':
        std::cout<<"Argument : Unknown argument option = " << optopt << std::endl;
        break;      
    }
  }
    
      

  // open the json config file
  std::string configPath(cPath);
  std::cout<<"\n\nConfig Path : "<<configPath<<std::endl;
  json myConfig = openJsonFile(configPath.c_str());
  std::cout<<"this"<<std::endl;
  
  // output directory for running
  std::string outputPath;
  if(oPath){
    outputPath = oPath;
    outputPath += "/";
    std::cout<<"\n\nOutput Path : "<<outputPath<<std::endl;
    int dir_err = 0;

    // if enabled you will remove the output directory to start freshly
    if(forceRemove){
      dir_err = system( (std::string("rm -r ")+outputPath).c_str() );
      if(dir_err == -1){
        std::cout<<"Error removing directory : "<<outputPath<<std::endl;
        return 1;
      }
    }

    // creation of output directory
    dir_err = system( (std::string("mkdir -p ")+outputPath).c_str() );
    if(dir_err == -1){
      std::cout<<"Error creating directory : "<<outputPath<<std::endl;
      return 1;
    }
  }

  // ip address
  char  ip_addr_string[32] ;
  strcpy(ip_addr_string, std::string(myConfig["ip"]).c_str() ) ; // SIS3153 IP address
  std::cout<<"\nIP Address : "<<ip_addr_string<<std::endl;

  // vme base address
  std::string vme_base_address_str = std::string(myConfig["vme_base_address"]);
  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  std::cout<<"\nBase VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address<<std::endl;

  // make a new digitizer instance
  vx1730 *digitizer = new vx1730(ip_addr_string, vme_base_address);

  // test digitizer board interface
  digitizer->TestComm();
  
  
  
  
  if( strcmp(rType,"showconfig")==0 ){
    INFO("Dumping the Current Config");
    digitizer->DumpConfig();
    INFO("NEventsInBuffer : "<<digitizer->DumpEventCount(false));
  }
  else if( strcmp(rType,"stoprun")==0 ){
    INFO("Stopping run after clearing buffer");
    digitizer->StopAcquisition(true);
    std::cout<<"NEvents BeforeRead : "<<std::dec<<digitizer->DumpEventCount( false )<<std::endl;
    while(digitizer->DumpEventCount( false )){
      std::cout<<"NEv1 : "<<digitizer->DumpEventCount( false )<<std::endl;
      digitizer->DumpFrontEvent( "delme.txt", DumpMode::New, true);
      std::cout<<"NEv2 : "<<digitizer->DumpEventCount( false )<<std::endl;
    }
    std::cout<<"NEvents AfterRead : "<<std::dec<<digitizer->DumpEventCount( false )<<std::endl;
  }
  else if( strcmp(rType,"waitfortrigger")==0 ){
    INFO("Waiting for triggers (for things like TLB testing)");
    digitizer->Configure(myConfig, true);  
    digitizer->StartAcquisition(true);
    
    int count=0;
    while(true){
      std::cout<<"Waiting : "<<std::dec<<count<<" - exit with CTRL+C"<<std::endl;
      count++;
      Wait(1.0);
    }
    
    digitizer->StopAcquisition(true);
  }
  else if( strcmp(rType,"swtrigger")==0 ){
    INFO("Getting SW triggers");
    digitizer->Configure(myConfig, true);  
    digitizer->StartAcquisition(true);
    
    // check that the input arguments align with what is expected
    if(nAcquire==-1){
      std::cout<<"You didn't provide a number of events to acquire"<<std::endl;
      return 9;
    }
    
    for(int iEv=0; iEv<nAcquire; iEv++){
      INFO("Count : "<<iEv);
      Wait(1.0);
      digitizer->SendSWTrigger();
      
      INFO("Get the Event");
      uint32_t raw_payload[MAXFRAGSIZE];
      digitizer->ReadRawEvent( raw_payload, true );
    }
    digitizer->StopAcquisition(true);
  }
  else if( strcmp(rType,"takedata")==0 ){
    INFO("Acquire N events, where N is configurable");
    
    // check that the input arguments align with what is expected
    if(nAcquire==-1){
      std::cout<<"You didn't provide a number of events to acquire"<<std::endl;
      return 9;
    }
    if(oPath==NULL){
      std::cout<<"You didn't provide an output path argument "<<std::endl;
      return 9;
    }
        
    INFO("CONFIG before configuration");
    digitizer->DumpConfig();
    
    digitizer->Configure(myConfig, true);  
    
    INFO("CONFIG before startAcquire");
    digitizer->DumpConfig();
    
    digitizer->StartAcquisition(true);
    
    INFO("CONFIG after startacquire");
    digitizer->DumpConfig();
    
    INFO("The digitizer is now acquiring data ...");

    while( digitizer->DumpEventCount() < nAcquire ){
      std::cout<<"Acquired NEvents = "<<digitizer->DumpEventCount()<<"  Target = "<<nAcquire<<std::endl;
      std::cout<<"Waiting for 1 second"<<std::endl;
      Wait(1);
    }
    
    digitizer->StopAcquisition(true);
  
  
    WARNING("Writing data out until event buffer is empty");
    std::cout<<"The current event count is : "<<digitizer->DumpEventCount()<<std::endl;
  
    // single output file specified with -o input
    std::string outputFile = outputPath+"data.txt";
    std::cout<<"Writing data to : "<<outputFile<<std::endl;
    ofstream outfile;
    outfile.open(outputFile, ios::out);
    outfile<<"New Data Run : "<<GetDateVerboseString()<<std::endl<<std::endl;
    outfile.close();
 
    // keep looping while there are events in the buffer and append them
    while(digitizer->DumpEventCount() != 0){
      digitizer->DumpFrontEvent( outputFile, DumpMode::Append );
      std::cout<<"Events left to read : "<<digitizer->DumpEventCount()<<std::endl;
    }
    std::cout<<"The final event count is (should be 0) : "<<digitizer->DumpEventCount()<<std::endl;

    WARNING("The run and data readout should be finished");

  }
  else if( strcmp(rType,"dev")==0 ){
    INFO("DEVELOPMENT");
    
    uint32_t raw_payload[MAXFRAGSIZE];
    
    digitizer->Configure(myConfig, true);  
    digitizer->StartAcquisition(true);
    
    
    for(int i=0; i<100; i++){
    
      digitizer->SendSWTrigger();
      std::cout<<"EventsInBuffer : "<<digitizer->DumpEventCount()<<std::endl;
      digitizer->ReadRawEvent( raw_payload, false );
      std::cout<<"Counter : "<<ConvertIntToWord(raw_payload[2])<<std::endl;
      std::cout<<"BCID    : "<<ConvertIntToWord(raw_payload[3])<<std::endl;
      
      
      Wait(1.0);
  
    }

    
    digitizer->StopAcquisition(true);
  }
  else{
    ERROR("Not a valid runtype : "<<rType);
  }
  
  
  
  return 0;
}



