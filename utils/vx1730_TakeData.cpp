//============================================================================
// Name        : vx1730_Dev
// Author      : Sam Meehan
// Description : Executable for developing the DAQ readout of vx1730
//============================================================================

#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"

#include "Comm_vx1730.h"

#include "nlohmann/json.hpp"
using json = nlohmann::json;

int main(int argc, char *argv[])
{


  ofstream fout;
  fout.open("fileSm.txt", ios::out);
  fout<<"This is the first line"<<std::endl;
  fout.close();
  
  fout.open("fileSm.txt", ios::app);
  fout<<"This is the second/first line"<<std::endl;
  fout.close();




  // used to tag the name of the program if you want in the output file
  std::string programName = argv[0];
  std::cout<<"Program Running : "<<programName<<std::endl;
  std::size_t pos = programName.find("/");
  std::string programTag = programName.substr(pos+1);
  std::cout<<"Program Tag     : "<<programTag<<std::endl;

  // require that you point the program at a config file
  // which will at least be used for the ip address and local base address
  if(argc<=1){
    std::cout<<"********************************************"<<std::endl;
    std::cout<<"Incorrect Usage : "<<std::endl;
    std::cout<<"Please check usage via the [-h] help option"<<std::endl;
    std::cout<<"********************************************"<<std::endl;
    return 1;
  }

  // argument parsing to get the path to the config file
  char *cPath = NULL;
  char *oPath = NULL;
  bool forceRemove = false;
  bool debug = false;
  int NAcquire=-1;
  int c;
  

  while ((c = getopt (argc, argv, "hc:o:f:d:n:")) != -1)
    switch (c)
      {
      case 'h':
        std::cout<<"\n\nThis is the help screen for your vx1730 digitizer command line running"<<std::endl;
        std::cout<<"You must provide the following arguments : "<<std::endl;
        std::cout<<" -c [CONFIGFILE] : CONFIGFILE is the path to the json formatted config file"<<std::endl;
        std::cout<<" -o [OUTPUTPATH] : OUTPUTPATH is the location of the single file where you will dump the data"<<std::endl;
        return 1;
        break;
      case 'c':
        cPath = optarg;
        break;
      case 'o':
        oPath = optarg;
        break;
      case 'f':
        forceRemove = true;
        break;
      case 'd':
        debug = true;
        break;
      case 'n':
        NAcquire = std::stoi(optarg);
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument. Path to the config file.\n", optopt);
        if (optopt == 'o')
          fprintf (stderr, "Option -%o requires an argument. Path to the output directory which will be replaced during running.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort();
      }


  // open the json config file
  std::string configPath(cPath);
  std::cout<<"\nConfig Path : "<<configPath<<std::endl;
  json myConfig = openJsonFile(configPath.c_str());

  // output directory for running
  std::string outputPath(oPath);
  std::cout<<"\nOutput Path : "<<outputPath<<std::endl;
  int dir_err = 0;

  // if enabled you will remove the output directory to start freshly
  if(forceRemove){
    dir_err = system( (std::string("rm ")+outputPath).c_str() );
    if(dir_err == -1){
      std::cout<<"Error removing directory : "<<outputPath<<std::endl;
      return 1;
    }
  }
  
  // the number of events to acquire
  std::cout<<"\nAcquiring NEvents : "<<NAcquire<<std::endl;
  


  // ip address
  char  ip_addr_string[32] ;
  strcpy(ip_addr_string, std::string(myConfig["ip"]).c_str() ) ; // SIS3153 IP address
  std::cout<<"\nIP Address : "<<ip_addr_string<<std::endl;

  // vme base address
  std::string vme_base_address_str = std::string(myConfig["vme_base_address"]);
  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  std::cout<<"\nBase VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address<<std::endl;

  // make a new digitizer instance
  vx1730 *digitizer = new vx1730(ip_addr_string, vme_base_address);

  // test digitizer board interface
  digitizer->TestComm();



  // the above is necessary configuration - below is where the functionality happens

  WARNING("Configuring digitizer and starting run");
  
  digitizer->DumpEventCount( debug );  
  digitizer->Configure( myConfig, debug);
  digitizer->StartAcquisition( debug );

  WARNING("The digitizer is now acquiring data ...");


  while( digitizer->DumpEventCount() < NAcquire ){
    std::cout<<"Acquired NEvents = "<<digitizer->DumpEventCount()<<"  Target = "<<NAcquire<<std::endl;
    std::cout<<"Waiting for 1 second"<<std::endl;
    Wait(1);
  }
  
  WARNING("Stopping run because you have enough events");
  digitizer->StopAcquisition( debug );
  WARNING("The run is stopped, so the lights on the digitizer should be off, are they?");

  WARNING("Writing data out until event buffer is empty");
  std::cout<<"The current event count is : "<<digitizer->DumpEventCount()<<std::endl;
  
  // single output file specified with -o input
  std::cout<<"Writing data to : "<<outputPath<<std::endl;
  ofstream outfile;
  outfile.open(outputPath, ios::out);
  outfile<<"New Data Run : "<<GetDateVerboseString()<<std::endl<<std::endl;
  outfile.close();
 
  // keep looping while there are events in the buffer and append them
  while(digitizer->DumpEventCount() != 0){
    digitizer->DumpFrontEvent( outputPath, DumpMode::Append );
    std::cout<<"Events left to read : "<<digitizer->DumpEventCount()<<std::endl;
  }
  std::cout<<"The final event count is (should be 0) : "<<digitizer->DumpEventCount()<<std::endl;


  WARNING("The run and data readout should be finished");

  return 0;
}



