//============================================================================
// Name        : sis3153eth_access_test.cpp
// Author      : th
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <sstream>

#include "Registers_sis3153.h"
#include "Registers_v1720.h"
#include "Registers_vx1730.h"

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"
#include "Helper_v1720.h"

using namespace std;

#include "nlohmann/json.hpp"
using json = nlohmann::json;

int main(int argc, char *argv[])
{

  // require that you point the program at a config file
  if(argc<=1){
    std::cout<<"You need to provide a config file input"<<std::endl;
    return 0;
  }

  // argument parsing to get the path to the config file
  char *cPath = NULL;
  int index;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "hc:")) != -1)
    switch (c)
      {
      case 'h':
        printHelp();
        break;
      case 'c':
        cPath = optarg;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument. Path to the config file.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort();
      }


  // open the json config file
  std::string configPath(cPath);
  std::cout<<"Config Path : "<<configPath<<std::endl;
  json myConfig = openJsonFile(configPath.c_str());

  // ip address
  char  ip_addr_string[32] ;
  strcpy(ip_addr_string, std::string(myConfig["ip"]).c_str() ) ; // SIS3153 IP address
  std::cout<<"IP Address : "<<ip_addr_string<<std::endl;

  // vme base address
  std::string vme_base_address_str = std::string(myConfig["vme_base_address"]);
  UINT vme_base_address = std::stoi(vme_base_address_str,0,16);
  std::cout<<"Base VME Address = 0x"<<std::setfill('0')<<std::setw(8)<<std::hex<<vme_base_address<<std::endl;

  //channel enable
  std::vector< json > channelConfig = myConfig["channel_config"];
  std::cout<<"NChannels : "<<channelConfig.size()<<std::endl;
  for(int iChan=0; iChan<channelConfig.size(); iChan++){
    std::cout<<"channelConfig : channel = "<<setw(10)<<channelConfig.at(iChan)["channel"]<<" | enable = "<<setw(10)<<channelConfig.at(iChan)["enable"]<<" | dc_offset = "<<setw(10)<<channelConfig.at(iChan)["dc_offset"]<<std::endl;
  }












  /////////////////////////////////
  // open the vme crate connection
  /////////////////////////////////
  sis3153eth *vme_crate;
  sis3153eth(&vme_crate, ip_addr_string);

  int return_code;
  char char_messages[128] ;
  unsigned int nof_found_devices ;

  return_code = vme_crate->vmeopen();  // open Vme interface
  vme_crate->get_vmeopen_messages(char_messages, &nof_found_devices);  // open Vme interface
  printf("get_vmeopen_messages = %s , nof_found_devices %d \n",char_messages, nof_found_devices);


  /////////////////////////////////
  // test interface of boards
  /////////////////////////////////
  sis3153_TestComm( vme_crate );
  v1720_TestComm( vme_crate, vme_base_address );

return 0;

  /////////////////////////////////
  // configure and start acquisition
  /////////////////////////////////
  v1720_Configure( vme_crate, vme_base_address, myConfig);

  v1720_StartAcquisition( vme_crate, vme_base_address );

  /////////////////////////////////
  // wait for manual input to perform trigger and readout
  /////////////////////////////////
  int x_in;
  std::cout<<"Waiting for input - press and button"<<std::endl;
  std::cin>>x_in;

  v1720_DumpEventCount( vme_crate, vme_base_address );

  v1720_SendSWTrigger( vme_crate, vme_base_address );

  v1720_DumpEventCount( vme_crate, vme_base_address );

  uint32_t raw_payload[MAXFRAGSIZE];
  v1720_ReadRawEvent( vme_crate, vme_base_address, raw_payload );

  std::cout<<"Printing only the header hardcoded"<<std::endl;
  for(int iWord=0; iWord<4; iWord++){
    std::cout<<"Word : "<<iWord<<"  "<<raw_payload[iWord]<<std::endl;
  }

  std::cout<<"Dumping raw payload"<<std::endl;
  Payload_Dump( raw_payload );

  v1720_DumpEventCount( vme_crate, vme_base_address );

  // create the EventFragmet object
  int payload_size = Payload_GetEventSize( raw_payload );
  const int total_size = sizeof(EventFragmentHeader) + sizeof(uint8_t) * payload_size;

  std::cout<<"Raw Payload size           : "<<payload_size<<std::endl;
  std::cout<<"Total EventFragment size   : "<<total_size<<std::endl;

  // event fragment creation
  std::unique_ptr<EventFragment>data((EventFragment *)malloc(total_size));



  std::cout<<"\n\nOUTSIDE"<<std::endl;
  std::cout<<"Header : marker         - "<<std::hex<<unsigned(data->header.marker)         <<std::endl;



  /////////////////////////////////
  // STOP data acquisition
  /////////////////////////////////
  v1720_StopAcquisition( vme_crate, vme_base_address );


  return 0;
}































